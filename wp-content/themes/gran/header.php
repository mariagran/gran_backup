<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gran
 */
global $configuracao;

// VERIFICAÇÃO MENU
if (get_page_link() != get_home_url()."/" && get_page_link() != get_home_url()."/en/" ) {
	$formatacao_header = "hand-header-internas";
}


?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NZW6B75');</script>
	<!-- End Google Tag Manager -->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NZW6B75"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header class="<?php if($formatacao_header != ""){echo $formatacao_header;}; ?>">
	
	<div class="header-content-area">
		<div class="row hand-containerFull">
			<div class="col-sm-1">
				<?php if (trim($configuracao['gran_header_logo']['url'])): ?>
				<figure>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<img src="<?php echo $configuracao['gran_header_logo']['url']; ?>" alt="<?php echo bloginfo(); ?>">
						<figcaption class="hidden"><?php echo bloginfo(); ?></figcaption>	
					</a>
				</figure>
				<?php endif; ?>
			</div>	
			<div class="col-sm-11">
				<div class="hand-button-menu-mobile">
					<button></button>
				</div>
				<div class="hand-menu-home">
					<ul class="menu-nav">
					<?php 
						
						wp_nav_menu( array('menu' => "Header Principal",));
						
					?>
					</ul>
				</div>
			</div>	
		</div>
	</div>

	<div class="hand-mega-menu">
		<div class="row">
			<div class="col-sm-7">	
				<ul class="menu-navegation">
					<li class="home">
						<a href="<?php echo esc_url( home_url( '/servicos/e-commerce-marketing/' ) ); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/img/strategy-menu.svg">
							<h2>Strategy</h2>
							<p>E-commerce Marketing</p>
							<p>Inbound Marketing</p>
							<p>Mídias Pagas</p>
							<p>Social Media Marketing</p>
						</a>
					</li>
					<li>
						<a href="<?php echo esc_url( home_url( '/servicos/design-naming/' ) ); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/img/desing.svg">
							<h2>Design</h2>
							<p>Naming</p>
							<p>Branding Concept</p>
							<p>Design de Interface</p>
						</a>
					</li>
					<li>
						<a href="<?php echo esc_url( home_url( '/servicos/websites/' ) ); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/img/dev.svg">
							<h2>Dev</h2>
							<p>Website</p>
							<p>Fast Sites</p>
							<p>Implantação e-commerce</p>
						</a>
					</li>
				</ul>
			</div>
			<div class="col-sm-5">
				<div class="hand-contato-float-menu">
					<ul>
						<li class="mobile-item-menu">
							<a href="<?php echo esc_url( home_url( '/servicos/e-commerce-marketing/' ) ); ?>">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cafe.svg">Strategy
							</a>
						</li>
						<li class="mobile-item-menu">
							<a href="<?php echo esc_url( home_url( '/servicos/design-naming/' ) ); ?>">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cafe.svg">Design
							</a>
						</li>
						<li class="mobile-item-menu">
							<a href="">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cafe.svg">Dev
							</a>
						</li>
						<li>
							<a href="<?php echo esc_url( home_url( '/land-page/' ) ); ?>">
								<img src="http://gran.ag/wp-content/uploads/2020/03/book.svg">Estudo de caso
							</a>
						</li>
						<li>
							<a href="">
								<img src="<?php echo get_template_directory_uri(); ?>/img/heart.svg">O que fazemos		
							</a>
						</li>
						<li>
							<a href="<?php echo esc_url( home_url( '/gran/' ) ); ?>">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cafe.svg">A Agência
							</a>
						</li>
						<li>
							<a href="<?php echo esc_url( home_url( '/blog/' ) ); ?>">
								<img src="http://gran.ag/wp-content/uploads/2019/10/bookmark.svg">Blog
							</a>
						</li>
						<li>
							<a href="">
								<img src="http://gran.ag/wp-content/uploads/2020/03/send-1.svg">Contato
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</header>
