<?php

/**
 * Template Name: All abord 
 * Description: Página All abord
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gran
 */
	$vagas = new WP_Query( array( 'post_type' => 'vaga', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );

	get_header(); 
?>
	<div class="pg all-abord">
		<section class="hand-perguntas-frequentes-area">
			
			<div class="hand-titulo-page">
				<div class="hand-container">
					<h2><?php echo $configuracao['gran_config_allAbord_titulo'] ?></h2>
					<p><?php echo $configuracao['gran_config_allAbord_subTitulo'] ?></p>
				</div>
			</div>

			<article>
				<div class="row">
					
					<div class="col-sm-6">
						<figure>
							<img src="<?php echo $configuracao['gran_config_allAbord_imagem']['url']; ?>" alt="<?php echo get_the_title(); ?>">
						</figure>
					</div>
					<div class="col-sm-6">
						<div class="hand-area-conteudo formatacaoLista">
							<?php 
							if ( have_posts() ) : while( have_posts() ) : the_post();
								
								echo the_content();

							endwhile;endif; wp_reset_query(); ?>					
						</div>
					</div>

				</div>

			</article>

		</section>

		<section class="hand-vagasDisponiveis">
			<div class="row">
				<div class="col-sm-6">
					<figure>
						<img src="<?php echo $configuracao['gran_config_allAbord_localizacao_imagem']['url']; ?>" alt="<?php echo bloginfo(); ?>">
						<figcaption></figcaption>
					</figure>
					
				</div>
				<div class="col-sm-6">
					<div class="hand-title-localizacao">
						<h3><?php echo $configuracao['gran_config_allAbord_localizacao_titulo'] ?></h3>
					</div>
					<article class="hand-paragrafo-descricao-oqueFazemos">
						<p><?php echo $configuracao['gran_config_allAbord_localizacao_descricao'] ?></p>
					</article>
						
					<div class="hand-lista-vagas-disponiveis">
						<ul>	
							<?php while($vagas->have_posts()) : $vagas->the_post(); ?>
								<li class="hand-vaga> ">
									<a href="<?php echo get_permalink(); ?>">
									<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">  <?php echo get_the_title(); ?>
									</a>
								</li>
							<?php endwhile;wp_reset_query(); ?>			
						</ul>
					</div>
				</div>
			</div>				
		</section>

	</div>

<?php 

	get_footer();