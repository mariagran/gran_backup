<?php

/**
 * Template Name: Gran
 * Description: Gran
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gran
 */



get_header(); ?>

<div class="pg pg-gran">
	<div class="hand-titulo-page">
		<div class="hand-container">
			<span><?php echo $configuracao['gran_config_titulo_gran'] ?></span>
			<h2><?php echo $configuracao['gran_config_sub_titulo_gran'] ?></h2>
			<p><?php echo $configuracao['gran_config_description_gran'] ?></p>
		</div>
	</div>

	<section class="hand-conteudo-gran-area">
		
			<div class="row">
				<div class="col-sm-6 text-right">
					<div class="hand-conteudo-gran formatacaoLista">
						<?php echo $configuracao['gran_config_texto_gran_gran'] ?>
					</div>
				</div>
				<div class="col-sm-6">
					<figure>
						<img src="<?php echo $configuracao['gran_config_imagem_ilustrativa_gran']['url'] ?>" alt="<?php echo $configuracao['gran_config_imagem_ilustrativa_gran']['url'] ?>">
					</figure>
				</div>
			</div>
		
	</section>

	<div class="hand-containerFull">
		<section class="hand-info-description-gran">
			<div class="row">
				<div class="col-sm-6">
					<figure>
						<img src="<?php echo get_template_directory_uri(); ?>/img/coffee.svg" alt="<?php echo $configuracao['gran_config_box_titulo_gran'] ?>">
					</figure>

					<div class="hand-info-titulo-gran">
						<h3><?php echo $configuracao['gran_config_box_titulo_gran'] ?></h3>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="hand-info-description-gran-conteudo">
						<p><?php echo $configuracao['gran_config_box_texto_gran'] ?></p>
					</div>
				</div>
			</div>
		</section>
	</div>

	<section class="hand-que-fazemos-gran">
		<h3><?php echo $configuracao['gran_config_queFazemso_titulo_gran'] ?></h3>
		<p><?php echo $configuracao['gran_config_queFazemso_sub_titulo_gran'] ?></p>
		
	
		<div class="hand-que-fazemos-escription-gran">
			<div class="hand-container">
				<div class="row">
					<div class="col-sm-6">
						<div class="hand-que-fazemos-escription-gran-text text-top">
							<img class="icone" src="<?php echo $configuracao['gran_config_queFazemso_icone_esquerdo_gran']['url'] ?>" alt="<?php echo $configuracao['gran_config_queFazemso_icone_esquerdo_gran'] ?>['url']">
							<?php echo $configuracao['gran_config_queFazemso_texto_esquerdo_gran'] ?>
						</div>
					</div>
					<div class="col-sm-6 ">
						<div class="hand-que-fazemos-escription-gran-text background-right">
							<?php echo $configuracao['gran_config_queFazemso_texto_direito_gran'] ?>
						</div>
					</div>
				</div>
			</div>

			<div class="row row-bottom">
				<div class="col-sm-6">
					<div class="hand-que-fazemos-escription-gran-text img ">
						<figure>
							<img src="<?php echo $configuracao['gran_config_colaboracao_img_gran']['url'] ?>" alt="<?php echo $configuracao['gran_config_colaboracao_img_gran']['url'] ?>">
						</figure>
					</div>
				</div>
				<div class="col-sm-6 corrcaoPadding">
					<div class="hand-que-fazemos-escription-gran-text bottom-text">
						<?php echo $configuracao['gran_config_colaboracao_texto_gran'] ?>
					</div>
				</div>
			</div>
			
		</div>

	</section>

	<section class="hand-que-fazemos-gran-servicos">
		<div class="hand-container formatacaoLista">
			<div class="row">
				<div class="col-sm-4">
					<div>
						<figure>
							<img src="<?php echo get_template_directory_uri(); ?>/img/strategyServ.svg">
							<figcaption class="hidden">Logo</figcaption>	
						</figure>
						<h2>Strategy</h2>
						<p>Desenvolvemos campanhas de mídia<br> digital integrada usando uma combinação <br>de Search, display, mídia  social, email, e <br> todas as outras ferramentas disponíveis.</p>
						<ul>
							<li>E-commerce Marketing</li>
							<li>Inbound Marketing</li>
							<li>Mídias Pagas</li>
							<li>Social Media Marketing</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-4">
					<div>
						<figure>
							<img src="<?php echo get_template_directory_uri(); ?>/img/featherServ.svg">
							<figcaption class="hidden">Logo</figcaption>	
						</figure>
						<h2>DESIGN</h2>
						<p>Encontramos a verdade sobre como<br> nossos clientes enriquecem a vida de seus<br> clientes. Obtemos uma visão do que os <br>clientes realmente desejam e de como<br> nossos clientes preenchem esses desejos <br>de maneira exclusiva.</p>
						<ul>
							<li>Identidade Visual</li>
							<li>Revitalização de marca</li>
							<li>Design de Interface</li>
							<li>Wireframing e Prototipagem</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-4">
					<div>
						<figure>
							<img src="<?php echo get_template_directory_uri(); ?>/img/codeServ.svg">
							<figcaption class="hidden">Logo</figcaption>	
						</figure>
						<h2>DEVELOPMENT</h2>
						<p>Transformamos o seu site em uma<br> máquina geradora de leads.<br> Desenvolvemos aplicações interativas<br> e dinâmicas seguindo as regras de<br> indexação de buscadores.</p>
						<ul>
							<li>Implantação E-commerce</li>
							<li>Hotsites</li>
							<li>Web Sites</li>
							<li>Fast Sites</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="hand-containerLarge">
		<section class="hand-info-prod">
			<div class="row">
				<div class="col-sm-6">
					<h3><?php echo $configuracao['gran_config_frase_rodape_gran'] ?></h3>
				</div>			
				<div class="col-sm-6 text-right">
					<a class="left" href="<?php echo $configuracao['gran_config_frase_link_gran_job'] ?>">Trabalhar na Gran</a>
					<a class="" href="<?php echo $configuracao['gran_config_frase_link_gran_contact'] ?>">Contratar a Gran</a>
				</div>			
			</div>
		</section>
	</div>

</div>
<?php get_footer();