<?php

/**
 * Template Name: Perguntas Frequentes
 * Description: Página Perguntas Frequentes
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gran
 */

$listaPostLeft  = array();
$listaPostRight = array();
$perguntas 		=   new WP_Query( 
							array( 
								'post_type' => 'perguntas_frequentes', 
								'orderby' => 'id', 
								'order' => 'asc', 
								'posts_per_page' => -1
							)
			 			);
$i = 0;
while($perguntas->have_posts()): $perguntas->the_post();
	
	$name        = get_the_title();
	$descripiton = get_the_content();
	
	if($i % 2 == 0){

		array_push($listaPostLeft,
			array( 
				"nome"        => $name,
				"descripiton" => $descripiton,
			)
		);

	}else{
		
		array_push($listaPostRight,
			array( 
				"nome"        => $name,
				"descripiton" => $descripiton,
			)
		);
	}
$i++; 
endwhile;

get_header(); ?>

<!-- PÁGINA PERGUNTAS FREQUENTES -->
<div class="pg hand-perguntas-frequentes">
	<section class="hand-perguntas-frequentes-area">
		<div class="hand-titulo-page">
			<div class="hand-container">
				<h2><?php 
							if ( have_posts() ) : while( have_posts() ) : the_post();
								
								echo get_the_title();

							endwhile;endif; wp_reset_query(); ?>			</h2>
			</div>
		</div>

		<article class="hand-container">
			<div class="row">
				<div class="col-sm-6">
					<div class="hand-conteudo">
						<?php foreach ($listaPostLeft as $listaPostLeft) :?>
						<h2><?php echo $listaPostLeft ['nome'] ?></h2>
						<p><?php echo $listaPostLeft ['descripiton'] ?></p>
						<?php endforeach; ?>
                        
					</div>
				</div>
				<div class="col-sm-6">
					<div class="hand-conteudo">
						<?php foreach ($listaPostRight as $listaPostRight) :?>
						<h2><?php echo $listaPostRight ['nome'] ?></h2>
						<p><?php echo $listaPostRight ['descripiton'] ?></p>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</article>
	</section>
</div>
<?php get_footer();