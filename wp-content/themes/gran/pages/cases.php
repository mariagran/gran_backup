<?php

/**
 * Template Name: Cases
 * Description: Cases
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gran
 */

get_header(); ?>

<div class="pg hand-quem-somos">
	<section class="hand-destaque-quem-somos areaDestaqueSombra ">
		<div class="hand-container">
			<h6 class="hidden">E-books</h6>
			<div class="hand-tittulo-destaque">
				<span><?php echo $configuracao['gran_config_titulo_land_page'] ?></span>
				<h2><?php echo $configuracao['gran_config_sub_titulo_land_page'] ?></h2>
				<a href="<?php echo $configuracao['gran_config_sub_link_land_page'] ?>">Entre em contato</a>
			</div>
		</div>
	</section>

	<div class="hand-container">
		<div class="row">
			<div class="col-sm-6">
				<article class="formatacaoLista">
					<?php echo $configuracao['gran_config_texto_land_page'] ?>
				</article>
			</div>
			<div class="col-sm-6">
				<div class="formulario">
					<div>
						<?php echo do_shortcode('[contact-form-7 id="137" title="Contato Land Page"]'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	
<?php get_footer();