<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gran
 */

get_header();
?>

<div class="pg hand-blog">
	<?php 
		//LOOP DE POST SERVIÇOS
		/* Start the Loop */
		$i = 0;
		while ( have_posts() ) :the_post();	
			if($i == 0){
	 ?>
	<section class="hand-blog-destaque">
		<div class="row">
			<div class="col-md-6">
				
				<article>
					<p>Filmes</p>
					<h2><?php echo get_the_title(); ?></h2>
					<a href="<?php echo get_permalink(); ?>">Saiba mais</a>
				</article>
			</div>
			<div class="col-md-6">
				<figure>
					<a href="<?php echo get_permalink(); ?>">
						<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title(); ?>">
					</a>
				</figure>
			</div>
		</div>
	</section>
	<?php }; $i++;endwhile; wp_reset_query(); ?>

	<div class="fundoBranco">

		<section class="hand-post-blog">
			<div class="hand-containerLarge">
				<div class="topo">
					<div class="divSelect">
						<select>
							<option>Filtrar por</option>
							<option>Primeiro</option>
							<option>Segundo</option>
						</select>
					</div>
					<h3>Últimas postagem</h3>
				</div>
				<ul class="grid-post">
				<?php
					/* Start the Loop */
					while ( have_posts() ) :the_post();	?>
					<li class="single-post">
						<figure>
							<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>">
						</figure>
						<span>10 Nov 2019</span>
						<h2><?php echo get_the_title(); ?></h2>
						<div class="icones">
							<ul>
								<li>
									<figure>
										<img src="<?php echo get_template_directory_uri(); ?>/img/heart.svg">
									</figure>
									<div class="quantidade">
										<span>1.323</span>
									</div>
								</li>
								<li><figure>
										<img src="<?php echo get_template_directory_uri(); ?>/img/message-square.png">
									</figure>
									<div class="quantidade">
										<span>23</span>
									</div>
								</li>
								<li class="ultimo-item">
									<figure>
										<img src="<?php echo get_template_directory_uri(); ?>/img/bookmark.svg">
									</figure>
								</li>
							</ul>
						</div>
					</li>
					<?php  endwhile; ?>
				</ul>
			
		</section>

		<div class="hand-paginador">

				<div class="row">
					<div class="col-sm-5 text-right">
						<div class="hand-btn-left">
							<a href="">
								<span>1/15</span>
								<p>Página anterior</p>
							</a>
						</div>
					</div>	
					<div class="col-sm-7">
						<div class="hand-btn-right">
							<a href="">
								<span>1/15</span>
								<p>próxima página</p>
							</a>
						</div>
					</div>	
				</div>	
			
		</div>
	</div>
</div>	

<?php
get_footer();
