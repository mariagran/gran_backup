<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gran
 */

get_header();
?>
<div class="pg hand-post">
			
	<section class="hand-title-sessao-post">
		<h1 class="hidden"><?php echo get_the_title(); ?></h1>

		<div class="row">
			<div class="col-md-6">
				<figure>
					<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title(); ?>" alt="<?php echo get_the_title(); ?>">
				</figure>
			</div>
			<div class="col-md-6">
				<div class="hand-title-post">
					<span>Filmes</span>
					<p><?php echo get_the_title(); ?></p>
				 	<?php $user = wp_get_current_user(); if($user): $userdata = get_user_meta( 1 ); ?>
					<div class="hand-autor-post">
						<div class="row">
							<div class="col-xs-3 col-sm-1">
								<figure>
									<?php echo get_avatar($user->ID, $user->ID); // IMAGEM DO AUTOR ?> 
								</figure>
							</div>
							<div class="col-xs-9 col-sm-11">
								<div class="hand-info-autor">
									<h2><?php echo $user->display_name; //NOME DO AUTOR ?></h2>
									<p><?php  echo $userdata['description'][0]; ?></p>
								</div>
							</div>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>

	<div class="fundoBranco">
		<div class="hand-containerLarge">
			<div class="row">
				<div class="col-sm-2">
					<div class="hand-redes-sociais">
						<ul>
							<li><a class="twitter"	href=""></a></li>
							<li><a class="facebook" href=""></a></li>
							<li><a class="links" href=""></a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-10">
				
					<section class="hand-content-conteudo">
						<?php while (have_posts()):the_post();echo the_content(); endwhile; ?>
					</section>

					<section class="hand-comentarios-posts">
						<!-- <h6>Deixe um comentário</h6> -->
						<!-- <p>O seu endereço de e-mail não será publicado. Campos obrigatórios são marcados com *</p> -->

						<!-- <form>
							<input type="" placeholder="Adicionar comentário…" name="">
						</form> -->

						<?php 
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
						    comments_template();
						endif; 
					?>

					</section>

					
				</div>
			</div>
			
		</div>

		<section class="hand-post-blog">
			<h3>Posts relacionados</h3>

		</section>
		<?php 

				// TEMPLATE BLOG
				include (TEMPLATEPATH . '/templates/blog.php'); 
			?>
		
	</div>

	

</div>	

<?php
get_footer();
