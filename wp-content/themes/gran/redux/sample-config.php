<?php
/**
 * ReduxFramework Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */
    /*********************************************
                //FUNÇÕES//
    **********************************************/
        if ( ! class_exists( 'Redux' ) ) {
            return;
        }


        // This is your option name where all the Redux data is stored.
        $opt_name = "configuracao";

        // This line is only for altering the demo. Can be easily removed.
        //$opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

        /*
         *
         * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
         *
         */

        $sampleHTML = '';
        if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
            Redux_Functions::initWpFilesystem();

            global $wp_filesystem;

            $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
        }

        // Background Patterns Reader
        $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
        $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
        $sample_patterns      = array();

        if ( is_dir( $sample_patterns_path ) ) {

            if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
                $sample_patterns = array();

                while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                    if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                        $name              = explode( '.', $sample_patterns_file );
                        $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                        $sample_patterns[] = array(
                            'alt' => $name,
                            'img' => $sample_patterns_url . $sample_patterns_file
                        );
                    }
                }
            }
        }

        $theme = wp_get_theme(); // For use with some settings. Not necessary.

        $args = array(
            // TYPICAL -> Change these values as you need/desire
            'opt_name'             => $opt_name,
            // This is where your data is stored in the database and also becomes your global variable name.
            'display_name'         => $theme->get( 'Name' ),
            // Name that appears at the top of your panel
            'display_version'      => $theme->get( 'Version' ),
            // Version that appears at the top of your panel
            'menu_type'            => 'menu',
            //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
            'allow_sub_menu'       => true,
            // Show the sections below the admin menu item or not
            'menu_title'           => __( 'Editar informações do site', 'redux-framework-demo' ),
            'page_title'           => __( 'Editar informações do site', 'redux-framework-demo' ),
            // You will need to generate a Google API key to use this feature.
            // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
            'google_api_key'       => '',
            // Set it you want google fonts to update weekly. A google_api_key value is required.
            'google_update_weekly' => false,
            // Must be defined to add google fonts to the typography module
            'async_typography'     => true,
            // Use a asynchronous font on the front end or font string
            //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
            'admin_bar'            => true,
            // Show the panel pages on the admin bar
            'admin_bar_icon'       => 'dashicons-portfolio',
            // Choose an icon for the admin bar menu
            'admin_bar_priority'   => 50,
            // Choose an priority for the admin bar menu
            'global_variable'      => '',
            // Set a different name for your global variable other than the opt_name
            'dev_mode'             => false,
            // Show the time the page took to load, etc
            'update_notice'        => true,
            // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
            'customizer'           => true,
            // Enable basic customizer support
            //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
            //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

            // OPTIONAL -> Give you extra features
            'page_priority'        => null,
            // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
            'page_parent'          => 'themes.php',
            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
            'page_permissions'     => 'manage_options',
            // Permissions needed to access the options panel.
            'menu_icon'            => '',
            // Specify a custom URL to an icon
            'last_tab'             => '',
            // Force your panel to always open to a specific tab (by id)
            'page_icon'            => 'icon-themes',
            // Icon displayed in the admin panel next to your menu_title
            'page_slug'            => '',
            // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
            'save_defaults'        => true,
            // On load save the defaults to DB before user clicks save or not
            'default_show'         => false,
            // If true, shows the default value next to each field that is not the default value.
            'default_mark'         => '',
            // What to print by the field's title if the value shown is default. Suggested: *
            'show_import_export'   => true,
            // Shows the Import/Export panel when not used as a field.

            // CAREFUL -> These options are for advanced use only
            'transient_time'       => 60 * MINUTE_IN_SECONDS,
            'output'               => true,
            // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
            'output_tag'           => true,
            // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
            // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

            // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
            'database'             => '',
            // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
            'use_cdn'              => true,
            // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

            // HINTS
            'hints'                => array(
                'icon'          => 'el el-question-sign',
                'icon_position' => 'right',
                'icon_color'    => 'lightgray',
                'icon_size'     => 'normal',
                'tip_style'     => array(
                    'color'   => 'red',
                    'shadow'  => true,
                    'rounded' => false,
                    'style'   => '',
                ),
                'tip_position'  => array(
                    'my' => 'top left',
                    'at' => 'bottom right',
                ),
                'tip_effect'    => array(
                    'show' => array(
                        'effect'   => 'slide',
                        'duration' => '500',
                        'event'    => 'mouseover',
                    ),
                    'hide' => array(
                        'effect'   => 'slide',
                        'duration' => '500',
                        'event'    => 'click mouseleave',
                    ),
                ),
            )
        );

        // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
        $args['admin_bar_links'][] = array(
            'id'    => 'redux-docs',
            'href'  => 'http://docs.reduxframework.com/',
            'title' => __( 'Documentation', 'redux-framework-demo' ),
        );
        $args['admin_bar_links'][] = array(
            //'id'    => 'redux-support',
            'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
            'title' => __( 'Support', 'redux-framework-demo' ),
        );
        $args['admin_bar_links'][] = array(
            'id'    => 'redux-extensions',
            'href'  => 'reduxframework.com/extensions',
            'title' => __( 'Extensions', 'redux-framework-demo' ),
        );
        // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
        $args['share_icons'][] = array(
            'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
            'title' => 'Visit us on GitHub',
            'icon'  => 'el el-github'
            //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
        );
        $args['share_icons'][] = array(
            'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
            'title' => 'Like us on Facebook',
            'icon'  => 'el el-facebook'
        );
        $args['share_icons'][] = array(
            'url'   => 'http://twitter.com/reduxframework',
            'title' => 'Follow us on Twitter',
            'icon'  => 'el el-twitter'
        );
        $args['share_icons'][] = array(
            'url'   => 'http://www.linkedin.com/company/redux-framework',
            'title' => 'Find us on LinkedIn',
            'icon'  => 'el el-linkedin'
        );

        // Panel Intro text -> before the form
        if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
            if ( ! empty( $args['global_variable'] ) ) {
                $v = $args['global_variable'];
            } else {
                $v = str_replace( '-', '_', $args['opt_name'] );
            }
            $args['intro_text'] = sprintf( __( '', 'redux-framework-demo' ) );
        } else {
            $args['intro_text'] = __( '', 'redux-framework-demo' );
        }

        // Add content after the form.
        $args['footer_text'] = __( '<p></p>', 'redux-framework-demo' );

        Redux::setArgs( $opt_name, $args );

        /*
         * ---> END ARGUMENTS
         */


        /*
         * ---> START HELP TABS
         */

        $tabs = array(
            array(
                'id'      => 'redux-help-tab-1',
                'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
                'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
            ),
            array(
                'id'      => 'redux-help-tab-2',
                'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
                'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
            )
        );
        Redux::setHelpTab( $opt_name, $tabs );

        // Set the help sidebar
        $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
        Redux::setHelpSidebar( $opt_name, $content );


        /*
         * <--- END HELP TABS
         */


        /*
         *
         * ---> START SECTIONS
         *
         */

    /*********************************************
              CAMPOS PERSONALIZADOS
    **********************************************/
        
        //SESSÃO HEADER CONFIGURAÇÃO
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Configurações do site', 'redux-framework-demo' ),
            'id'               => 'gran_configuracoes_site',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Configurações Site', 'redux-framework-demo' ),
                    'id'               => 'config_site',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'gran_header_logo',
                            'type'     => 'media',
                            'title'    => __( 'Logo', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_header_logo_footer',
                            'type'     => 'media',
                            'title'    => __( 'Logo footer', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                         array(
                            'id'       => 'gran_header_copyright',
                            'type'     => 'text',
                            'title'    => __( 'Copyright', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                      
                    )
                )
            );
            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Configurações contato', 'redux-framework-demo' ),
                    'id'               => 'config_site_contato',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'gran_telefone',
                            'type'     => 'text',
                            'title'    => __( 'Telefone', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_email',
                            'type'     => 'text',
                            'title'    => __( 'E-mail', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_endereco',
                            'type'     => 'text',
                            'title'    => __( 'Endereço', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                      
                    )
                )
            );
            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Configurações social', 'redux-framework-demo' ),
                    'id'               => 'config_site_social',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'gran_instagram',
                            'type'     => 'text',
                            'title'    => __( 'Instagram', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_facebook',
                            'type'     => 'text',
                            'title'    => __( 'Facebook', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_twitter',
                            'type'     => 'text',
                            'title'    => __( 'Twitter', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_linkedin',
                            'type'     => 'text',
                            'title'    => __( 'Linkedin', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ), 
                        array(
                            'id'       => 'gran_behance',
                            'type'     => 'text',
                            'title'    => __( 'Behance', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_dribbble',
                            'type'     => 'text',
                            'title'    => __( 'Dribbble', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                      
                    )
                )
            );
             // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Configurações rodapé', 'redux-framework-demo' ),
                    'id'               => 'config_site_rodape',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações rodapeé', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                         array(
                            'id'       => 'gran_gran',
                            'type'     => 'text',
                            'title'    => __( 'Menu GRAN', 'redux-framework-demo' ),
                            'desc'    => __( 'Para fazer alterações nos ítens, acesse a opção MENU', 'redux-framework-demo' ),
                        ), 
                        array(
                            'id'       => 'gran_solucao',
                            'type'     => 'text',
                            'title'    => __( 'Menu Soluções', 'redux-framework-demo' ),
                            'desc'    => __( 'Para fazer alterações nos ítens, acesse a opção MENU', 'redux-framework-demo' ),
                        ),
                         array(
                            'id'       => 'gran_contato',
                            'type'     => 'text',
                            'title'    => __( 'Entre em contato', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                         array(
                            'id'       => 'gran_contato_social',
                            'type'     => 'text',
                            'title'    => __( 'Soluções Social', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                      
                    )
                )
            );

        //SESSÃO CONFIGURAÇÃO PÁGINA INICIAL
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Configurações inicial', 'redux-framework-demo' ),
            'id'               => 'gran_configuracoes_initial',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Destaque', 'redux-framework-demo' ),
                    'id'               => 'gran_config_inicial_destaque',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'gran_config_inicial_destaque_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título destaque', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_destaque_texto',
                            'type'     => 'textarea',
                            'title'    => __( 'Texto', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                         array(
                            'id'       => 'gran_config_inicial_destaque_link',
                            'type'     => 'text',
                            'title'    => __( 'Link destaque', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                      
                    )
                )
            );

            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Destaque - English', 'redux-framework-demo' ),
                    'id'               => 'gran_config_inicial_destaque_US',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'gran_config_inicial_destaque_titulo_US',
                            'type'     => 'text',
                            'title'    => __( 'Título destaque English', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_destaque_texto_US',
                            'type'     => 'textarea',
                            'title'    => __( 'Texto English', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                         array(
                            'id'       => 'gran_config_inicial_destaque_link_US',
                            'type'     => 'text',
                            'title'    => __( 'Link destaque English', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                      
                    )
                )
            );

            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'O que fazemos', 'redux-framework-demo' ),
                    'id'               => 'gran_config_inicial_oqueFazemos',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'gran_config_inicial_oqueFazemos_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título o que fazemos', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_oqueFazemos_texto',
                            'type'     => 'textarea',
                            'title'    => __( 'Texto', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                      
                    )
                )
            );

            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'O que fazemos - English', 'redux-framework-demo' ),
                    'id'               => 'gran_config_inicial_oqueFazemos_US',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'gran_config_inicial_oqueFazemos_titulo_US',
                            'type'     => 'text',
                            'title'    => __( 'Título o que fazemos - English', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_oqueFazemos_texto_US',
                            'type'     => 'textarea',
                            'title'    => __( 'Texto - English', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                      
                    )
                )
            );

            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Serviços', 'redux-framework-demo' ),
                    'id'               => 'gran_config_inicial_servicos',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'gran_config_inicial_servicos_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Serviço - Título - 1', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_servicos_texto',
                            'type'     => 'textarea',
                            'title'    => __( 'Serviço - Texto - 1', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_servicos_icone',
                            'type'     => 'media',
                            'title'    => __( 'Serviço - ícone - 1', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_servicos_link1',
                            'type'     => 'text',
                            'title'    => __( 'Link serviço - ícone - 1', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_servicos_titulo_2',
                            'type'     => 'text',
                            'title'    => __( 'Serviço - Título - 2', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_servicos_texto_2',
                            'type'     => 'textarea',
                            'title'    => __( 'Serviço - Texto - 2', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_servicos_icone_2',
                            'type'     => 'media',
                            'title'    => __( 'Serviço - ícone - 2', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                         array(
                            'id'       => 'gran_config_inicial_servicos_link2',
                            'type'     => 'text',
                            'title'    => __( 'Link serviço - ícone - 2', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_servicos_titulo_3',
                            'type'     => 'text',
                            'title'    => __( 'Serviço - Título - 3', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_servicos_texto_3',
                            'type'     => 'textarea',
                            'title'    => __( 'Serviço - Texto - 3', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_servicos_icone_3',
                            'type'     => 'media',
                            'title'    => __( 'Serviço - ícone - 3', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                         array(
                            'id'       => 'gran_config_inicial_servicos_link3',
                            'type'     => 'text',
                            'title'    => __( 'Link serviço - ícone - 3', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Serviços - English', 'redux-framework-demo' ),
                    'id'               => 'gran_config_inicial_servicos_English',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'gran_config_inicial_servicos_titulo_English',
                            'type'     => 'text',
                            'title'    => __( 'Serviço - Título - 1', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_servicos_texto_English',
                            'type'     => 'textarea',
                            'title'    => __( 'Serviço - Texto - 1', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_servicos_icone_English',
                            'type'     => 'media',
                            'title'    => __( 'Serviço - ícone - 1', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_servicos_titulo_2_English',
                            'type'     => 'text',
                            'title'    => __( 'Serviço - Título - 2', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_servicos_texto_2_English',
                            'type'     => 'textarea',
                            'title'    => __( 'Serviço - Texto - 2', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_servicos_icone_2_English',
                            'type'     => 'media',
                            'title'    => __( 'Serviço - ícone - 2', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_servicos_titulo_3_English',
                            'type'     => 'text',
                            'title'    => __( 'Serviço - Título - 3', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_servicos_texto_3_English',
                            'type'     => 'textarea',
                            'title'    => __( 'Serviço - Texto - 3', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_servicos_icone_3_English',
                            'type'     => 'media',
                            'title'    => __( 'Serviço - ícone - 3', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Texto Informativo', 'redux-framework-demo' ),
                    'id'               => 'gran_config_inicial_info',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'gran_config_inicial_info_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Texto Informativo - Título', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_info_texto',
                            'type'     => 'textarea',
                            'title'    => __( 'Texto Informativo', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Texto Informativo - English', 'redux-framework-demo' ),
                    'id'               => 'gran_config_inicial_info_English',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'gran_config_inicial_info_titulo_English',
                            'type'     => 'text',
                            'title'    => __( 'Texto Informativo - Título', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_inicial_info_texto_English',
                            'type'     => 'textarea',
                            'title'    => __( 'Texto Informativo', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Parceiros', 'redux-framework-demo' ),
                    'id'               => 'gran_config_inicial_parceiros',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'gran_config_inicial_parceiro_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Parceiros - Título', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ), 
                        array(
                            'id'       => 'gran_config_inicial_parceiro_titulo_english',
                            'type'     => 'text',
                            'title'    => __( 'Parceiros - Título English', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );
        
        //SESSÃO PÁGINA QUEM SOMOS
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Configurações  Quem Somos', 'redux-framework-demo' ),
            'id'               => 'gran_configuracoes_quemSomos',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Quem Somos Inicial', 'redux-framework-demo' ),
                    'id'               => 'gran_config_quemSomos_destaque',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        
                         array(
                            'id'       => 'gran_config_quemSomos_destaque_subTitulo',
                            'type'     => 'text',
                            'title'    => __( 'Subtítulo destaque', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_quemSomos_destaque_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título destaque', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_quemSomos_destaque_texto',
                            'type'     => 'textarea',
                            'title'    => __( 'Texto', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Fundamentos', 'redux-framework-demo' ),
                    'id'               => 'gran_config_quemSomos_fundamentos',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        
                       
                        array(
                            'id'       => 'gran_config_quemSomos_fundamentos_imagem',
                            'type'     => 'media',
                            'title'    => __( 'Imagem destaque', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_quemSomos_fundamentos_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_quemSomos_fundamentos_texto',
                            'type'     => 'textarea',
                            'title'    => __( 'Texto', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ), 
                        array(
                            'id'       => 'gran_config_quemSomos_fundamentos_subTitulo',
                            'type'     => 'text',
                            'title'    => __( 'SubTitulo', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Como trabalhamos?', 'redux-framework-demo' ),
                    'id'               => 'gran_config_quemSomos_comoTrabalhamos',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        
                        array(
                            'id'       => 'gran_config_quemSomos_comoTrabalhamos_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_quemSomos_comoTrabalhamos_texto',
                            'type'     => 'textarea',
                            'title'    => __( 'Texto', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ), 
                    )
                )
            );

            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'O que fazemos', 'redux-framework-demo' ),
                    'id'               => 'gran_config_quemSomos_QueFazemos',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        
                   
                        array(
                            'id'       => 'gran_config_quemSomos_QueFazemos_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_quemSomos_QueFazemos_subTitulo',
                            'type'     => 'text',
                            'title'    => __( 'SubTitulo', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_quemSomos_QueFazemos_tituloTextoEsquerda',
                            'type'     => 'text',
                            'title'    => __( 'Titulo texto da Esquerda', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_quemSomos_QueFazemos_textoEsquerda',
                            'type'     => 'textarea',
                            'title'    => __( 'Conteúdo do texto da Esquerda', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_quemSomos_QueFazemos_tituloTextoDireita',
                            'type'     => 'text',
                            'title'    => __( 'Titulo texto da Direira', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_quemSomos_QueFazemos_TextoDireita',
                            'type'     => 'textarea',
                            'title'    => __( 'Conteúdo texto da Direita', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ), 
                        
                    )
                )
            );
            
            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'O que fazemos Descricao', 'redux-framework-demo' ),
                    'id'               => 'gran_config_quemSomos_QueFazemos_Descricao',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        
                        array(
                            'id'       => 'gran_config_quemSomos_QueFazemos_Descricao_imagem',
                            'type'     => 'media',
                            'title'    => __( 'Imagem', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ), 
                        array(
                            'id'       => 'gran_config_quemSomos_QueFazemos_Descricao_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ), 
                        array(
                            'id'       => 'gran_config_quemSomos_QueFazemos_Descricao_subTitulo',
                            'type'     => 'text',
                            'title'    => __( 'subTítulo', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_quemSomos_QueFazemos_Descricao_texto',
                            'type'     => 'textarea',
                            'title'    => __( 'Texto', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ), 
                    )
                )
            );

        //SESSÃO PÁGINA ALL ABORD
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Configurações  All Abord', 'redux-framework-demo' ),
            'id'               => 'gran_configuracoes_allAbord',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'All Abord inicial', 'redux-framework-demo' ),
                    'id'               => 'gran_config_allAbord_destaque',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        
                         
                        array(
                            'id'       => 'gran_config_allAbord_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'gran_config_allAbord_subTitulo',
                            'type'     => 'text',
                            'title'    => __( 'Subtítulo', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_allAbord_imagem',
                            'type'     => 'media',
                            'title'    => __( 'Imagem Destaque All Abord', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                       
                    )
                )
            );

            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Sessão Vagas', 'redux-framework-demo' ),
                    'id'               => 'gran_config_allAbord_vaga',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        
                         
                        array(
                            'id'       => 'gran_config_allAbord_localizacao_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título Localização Hand', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'gran_config_allAbord_localizacao_descricao',
                            'type'     => 'textarea',
                            'title'    => __( 'Descrição Hand', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'gran_config_allAbord_localizacao_imagem',
                            'type'     => 'media',
                            'title'    => __( 'Imagem Destaque', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                       
                    )
                )
            );

         //VAGAS
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Configurações  Vagas', 'redux-framework-demo' ),
            'id'               => 'gran_configuracoes_vagas',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Vagas', 'redux-framework-demo' ),
                    'id'               => 'gran_config_vagas',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                         
                        array(
                            'id'       => 'gran_config_vagas_subtitulo',
                            'type'     => 'text',
                            'title'    => __( 'Subtítulo', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'gran_config_vagas_titulo_formulario',
                            'type'     => 'text',
                            'title'    => __( 'Título Formulario', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                         array(
                            'id'       => 'gran_config_vagas_script_formulario',
                            'type'     => 'text',
                            'title'    => __( 'Script Formulario', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                       
                    )
                )
            );
        //VAGAS
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Página Gran', 'redux-framework-demo' ),
            'id'               => 'gran_configuracoes_gran',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Gran', 'redux-framework-demo' ),
                    'id'               => 'gran_config_gran',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                         
                        array(
                            'id'       => 'gran_config_titulo_gran',
                            'type'     => 'text',
                            'title'    => __( 'Título Gran', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_sub_titulo_gran',
                            'type'     => 'text',
                            'title'    => __( 'Subtitulo Gran', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_description_gran',
                            'type'     => 'text',
                            'title'    => __( 'Descrição Gran', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                         array(
                            'id'       => 'gran_config_imagem_ilustrativa_gran',
                            'type'     => 'media',
                            'title'    => __( 'Foto Ilustrativa', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                       array(
                            'id'       => 'gran_config_texto_gran_gran',
                            'type'     => 'editor',
                            'title'    => __( 'Sobre a Gran', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_box_titulo_gran',
                            'type'     => 'text',
                            'title'    => __( 'Box cinza título', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_box_texto_gran',
                            'type'     => 'textarea',
                            'title'    => __( 'Box cinza texto', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_queFazemso_titulo_gran',
                            'type'     => 'text',
                            'title'    => __( 'O que fazemos?', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_queFazemso_sub_titulo_gran',
                            'type'     => 'text',
                            'title'    => __( 'O que fazemos?', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                         array(
                            'id'       => 'gran_config_queFazemso_icone_esquerdo_gran',
                            'type'     => 'media',
                            'title'    => __( 'O que fazemos? - Icone esquerdo', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                         array(
                            'id'       => 'gran_config_queFazemso_texto_esquerdo_gran',
                            'type'     => 'editor',
                            'title'    => __( 'O que fazemos? - Texto esquerdo', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_queFazemso_texto_direito_gran',
                            'type'     => 'editor',
                            'title'    => __( 'O que fazemos? - Texto direito', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_colaboracao_img_gran',
                            'type'     => 'media',
                            'title'    => __( 'Imagem -  Colaboração', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_colaboracao_texto_gran',
                            'type'     => 'editor',
                            'title'    => __( 'Imagem -  Texto', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_frase_rodape_gran',
                            'type'     => 'text',
                            'title'    => __( 'Frase rodapé', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_frase_link_gran_job',
                            'type'     => 'text',
                            'title'    => __( 'Link trabalhar na Gran', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                         array(
                            'id'       => 'gran_config_frase_link_gran_contact',
                            'type'     => 'text',
                            'title'    => __( 'Link contratar a Gran', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );
            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                'title'            => __( 'Gran English', 'redux-framework-demo' ),
                'id'               => 'gran_config_gran_english',
                'subsection'       => true,
                'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                'customizer_width' => '450px',
                'fields'           => array(

                    array(
                        'id'       => 'gran_config_titulo_gran_english',
                        'type'     => 'text',
                        'title'    => __( 'Título Gran', 'redux-framework-demo' ),
                        'desc'    => __( '', 'redux-framework-demo' ),
                    ),
                    array(
                        'id'       => 'gran_config_sub_titulo_gran_english',
                        'type'     => 'text',
                        'title'    => __( 'Subtitulo Gran', 'redux-framework-demo' ),
                        'desc'    => __( '', 'redux-framework-demo' ),
                    ),
                    array(
                        'id'       => 'gran_config_description_gran_english',
                        'type'     => 'text',
                        'title'    => __( 'Descrição Gran', 'redux-framework-demo' ),
                        'desc'    => __( '', 'redux-framework-demo' ),
                    ),
                     array(
                        'id'       => 'gran_config_imagem_ilustrativa_gran_english',
                        'type'     => 'media',
                        'title'    => __( 'Foto Ilustrativa', 'redux-framework-demo' ),
                        'desc'    => __( '', 'redux-framework-demo' ),
                    ),
                   array(
                        'id'       => 'gran_config_texto_gran_gran_english',
                        'type'     => 'editor',
                        'title'    => __( 'Sobre a Gran', 'redux-framework-demo' ),
                        'desc'    => __( '', 'redux-framework-demo' ),
                    ),
                    array(
                        'id'       => 'gran_config_box_titulo_gran_english',
                        'type'     => 'text',
                        'title'    => __( 'Box cinza título', 'redux-framework-demo' ),
                        'desc'    => __( '', 'redux-framework-demo' ),
                    ),
                    array(
                        'id'       => 'gran_config_box_texto_gran_english',
                        'type'     => 'textarea',
                        'title'    => __( 'Box cinza texto', 'redux-framework-demo' ),
                        'desc'    => __( '', 'redux-framework-demo' ),
                    ),
                    array(
                        'id'       => 'gran_config_queFazemso_titulo_gran_english',
                        'type'     => 'text',
                        'title'    => __( 'O que fazemos?', 'redux-framework-demo' ),
                        'desc'    => __( '', 'redux-framework-demo' ),
                    ),
                    array(
                        'id'       => 'gran_config_queFazemso_sub_titulo_gran_english',
                        'type'     => 'text',
                        'title'    => __( 'O que fazemos?', 'redux-framework-demo' ),
                        'desc'    => __( '', 'redux-framework-demo' ),
                    ),
                     array(
                        'id'       => 'gran_config_queFazemso_icone_esquerdo_gran_english',
                        'type'     => 'media',
                        'title'    => __( 'O que fazemos? - Icone esquerdo', 'redux-framework-demo' ),
                        'desc'    => __( '', 'redux-framework-demo' ),
                    ),
                     array(
                        'id'       => 'gran_config_queFazemso_texto_esquerdo_gran_english',
                        'type'     => 'editor',
                        'title'    => __( 'O que fazemos? - Texto esquerdo', 'redux-framework-demo' ),
                        'desc'    => __( '', 'redux-framework-demo' ),
                    ),
                    array(
                        'id'       => 'gran_config_queFazemso_texto_direito_gran_english',
                        'type'     => 'editor',
                        'title'    => __( 'O que fazemos? - Texto direito', 'redux-framework-demo' ),
                        'desc'    => __( '', 'redux-framework-demo' ),
                    ),
                    array(
                        'id'       => 'gran_config_colaboracao_img_gran_english',
                        'type'     => 'media',
                        'title'    => __( 'Imagem -  Colaboração', 'redux-framework-demo' ),
                        'desc'    => __( '', 'redux-framework-demo' ),
                    ),
                    array(
                        'id'       => 'gran_config_colaboracao_texto_gran_english',
                        'type'     => 'editor',
                        'title'    => __( 'Imagem -  Texto', 'redux-framework-demo' ),
                        'desc'    => __( '', 'redux-framework-demo' ),
                    ),
                    array(
                        'id'       => 'gran_config_frase_rodape_gran_english',
                        'type'     => 'text',
                        'title'    => __( 'Frase rodapé', 'redux-framework-demo' ),
                        'desc'    => __( '', 'redux-framework-demo' ),
                    ),
                    array(
                        'id'       => 'gran_config_frase_link_gran_job_english',
                        'type'     => 'text',
                        'title'    => __( 'Link trabalhar na Gran', 'redux-framework-demo' ),
                        'desc'    => __( '', 'redux-framework-demo' ),
                    ),

                     array(
                        'id'       => 'gran_config_frase_link_gran_contact_english',
                        'type'     => 'text',
                        'title'    => __( 'Link contratar a Gran', 'redux-framework-demo' ),
                        'desc'    => __( '', 'redux-framework-demo' ),
                    ),

                )
            ));
        
         //VAGAS
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Land Page', 'redux-framework-demo' ),
            'id'               => 'gran_configuracoes_land_page',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
             Redux::setSection( $opt_name, array(
                    'title'            => __( 'Gran', 'redux-framework-demo' ),
                    'id'               => 'gran_config_land_page',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                         
                        array(
                            'id'       => 'gran_config_titulo_land_page',
                            'type'     => 'text',
                            'title'    => __( 'Título Gran - Land Page', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_sub_titulo_land_page',
                            'type'     => 'text',
                            'title'    => __( 'Subtítulo Gran - Land Page', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'gran_config_sub_link_land_page',
                            'type'     => 'text',
                            'title'    => __( 'Link Gran - Land Page', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                         array(
                            'id'       => 'gran_config_texto_land_page',
                            'type'     => 'editor',
                            'title'    => __( 'Texto Gran - Land Page', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                       
                    )
                )
            );


    /*********************************************
                 //FUNÇÕES//
    **********************************************/
        Redux::setSection( $opt_name, array(
            'icon'            => 'el el-list-alt',
            'title'           => __( 'Customizer Only', 'redux-framework-demo' ),
            'desc'            => __( '<p class="description">This Section should be visible only in Customizer</p>', 'redux-framework-demo' ),
            'customizer_only' => true,
            'fields'          => array(
                array(
                    'id'              => 'opt-customizer-only',
                    'type'            => 'select',
                    'title'           => __( 'Customizer Only Option', 'redux-framework-demo' ),
                    'subtitle'        => __( 'The subtitle is NOT visible in customizer', 'redux-framework-demo' ),
                    'desc'            => __( 'The field desc is NOT visible in customizer.', 'redux-framework-demo' ),
                    'customizer_only' => true,
                    //Must provide key => value pairs for select options
                    'options'         => array(
                        '1' => 'Opt 1',
                        '2' => 'Opt 2',
                        '3' => 'Opt 3'
                    ),
                    'default'         => '2'
                ),
            )
        ) );




        /*
         * <--- END SECTIONS
         */


        /*
         *
         * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
         *
         */

        /*
        *
        * --> Action hook examples
        *
        */

        // If Redux is running as a plugin, this will remove the demo notice and links
        //add_action( 'redux/loaded', 'remove_demo' );

        // Function to test the compiler hook and demo CSS output.
        // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
        //add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

        // Change the arguments after they've been declared, but before the panel is created
        //add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

        // Change the default value of a field after it's been set, but before it's been useds
        //add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

        // Dynamically add a section. Can be also used to modify sections/fields
        //add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

        /**
         * This is a test function that will let you see when the compiler hook occurs.
         * It only runs if a field    set with compiler=>true is changed.
         * */
        if ( ! function_exists( 'compiler_action' ) ) {
            function compiler_action( $options, $css, $changed_values ) {
                echo '<h1>The compiler hook has run!</h1>';
                echo "<pre>";
                print_r( $changed_values ); // Values that have changed since the last save
                echo "</pre>";
                //print_r($options); //Option values
                //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
            }
        }

        /**
         * Custom function for the callback validation referenced above
         * */
        if ( ! function_exists( 'redux_validate_callback_function' ) ) {
            function redux_validate_callback_function( $field, $value, $existing_value ) {
                $error   = false;
                $warning = false;

                //do your validation
                if ( $value == 1 ) {
                    $error = true;
                    $value = $existing_value;
                } elseif ( $value == 2 ) {
                    $warning = true;
                    $value   = $existing_value;
                }

                $return['value'] = $value;

                if ( $error == true ) {
                    $return['error'] = $field;
                    $field['msg']    = 'your custom error message';
                }

                if ( $warning == true ) {
                    $return['warning'] = $field;
                    $field['msg']      = 'your custom warning message';
                }

                return $return;
            }
        }

        /**
         * Custom function for the callback referenced above
         */
        if ( ! function_exists( 'redux_my_custom_field' ) ) {
            function redux_my_custom_field( $field, $value ) {
                print_r( $field );
                echo '<br/>';
                print_r( $value );
            }
        }

        /**
         * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
         * Simply include this function in the child themes functions.php file.
         * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
         * so you must use get_template_directory_uri() if you want to use any of the built in icons
         * */
        if ( ! function_exists( 'dynamic_section' ) ) {
            function dynamic_section( $sections ) {
                //$sections = array();
                $sections[] = array(
                    'title'  => __( 'Section via hook', 'redux-framework-demo' ),
                    'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo' ),
                    'icon'   => 'el el-paper-clip',
                    // Leave this as a blank section, no options just some intro text set above.
                    'fields' => array()
                );

                return $sections;
            }
        }

        /**
         * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
         * */
        if ( ! function_exists( 'change_arguments' ) ) {
            function change_arguments( $args ) {
                //$args['dev_mode'] = true;

                return $args;
            }
        }

        /**
         * Filter hook for filtering the default value of any given field. Very useful in development mode.
         * */
        if ( ! function_exists( 'change_defaults' ) ) {
            function change_defaults( $defaults ) {
                $defaults['str_replace'] = 'Testing filter hook!';

                return $defaults;
            }
        }

        /**
         * Removes the demo link and the notice of integrated demo from the redux-framework plugin
         */
        if ( ! function_exists( 'remove_demo' ) ) {
            function remove_demo() {
                // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
                if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                    remove_filter( 'plugin_row_meta', array(
                        ReduxFrameworkPlugin::instance(),
                        'plugin_metalinks'
                    ), null, 2 );

                    // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                    remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
                }
            }
        }

