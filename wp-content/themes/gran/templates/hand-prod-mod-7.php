<div class="pg hand-prod-mod-5 hand-prod-mod-7">
			
	<section class="hand-destaque-prod">
		<img src="img/grid2.svg">
		<div class="hand-containerLarge">
			<article>
				<span class="hand-subTitle-topo"><?php echo $terms[0]->name; ?></span>
				<h1 class="hand-title-destaque"><?php echo get_the_title(); ?></h1>
				<p class="hand-paragrafo-deatque"><?php echo 'Nós criamos marcas'; ?></p>
			</article>

			<nav>
				<ul id="" class="carrosselMenuItemDesign owl-carousel">
					<?php while($servicos->have_posts()) : $servicos->the_post(); ?>
					<li class="item  <?php if($post_title == get_the_title()){ echo $ativo_item;}; ?> ">
						<a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>
					</li>
					<?php endwhile;wp_reset_query(); ?>
				</ul>
			</nav>
		</div>
	</section>

	<section class="hand-branding">
		
		<div class="row">
			<div class="col-md-6">
				<article class="formatacaoLista">
					<h2>Branding</h2>
					<h3>Não importa quão grande (ou pequeno) seja o seu negócio, o branding de sucesso é essencial - especialmente quando se trata de branding digital.</h3>
					<p>De iniciantes a negócios bem-sucedidos em escala empresarial, a GRAN se aprofunda na alma de uma marca para gerar estratégias que funcionem.  Nossa equipe ajudará você a definir sua identidade visual, implementar o design e preparar materiais de marketing atraentes que ampliem seu impacto.</p>
				</article>
				<figure class="template-mobile">
					<img src="<?php echo get_template_directory_uri() . '/img/oqueebranding.svg'; ?>" alt="Imagem o que é branding concept">
				</figure>
			</div>
			<div class="col-md-6">
				<figure class="template-desktop">
					<img src="<?php echo get_template_directory_uri() . '/img/oqueebranding.svg'; ?>" alt="Imagem o que é branding concept">
				</figure>
			</div>
		</div>

	</section>

	<section class="hand-brand-design">
		<div class="hand-containerLarge">
			<article>
				<h2>Brand design</h2>
				<div class="div-paragrafo">
					<p>O Logotipo é a representação visual que o mercado tem sobre você, seu negócio, em outras palavras, é ele quem representa sua empresa quando você não está, e convenhamos não conseguimos estar em todos os lugares 24h por dia, certo? Então de o devido valor ao seu Logo, ele 
					assume grande responsabilidade no seu sucesso. </p>
				</div>
				<div class="div-subtitulo">
					<h3>Marcas são compostas de atributos sinceros e perceptíveis, sejam eles tangíveis ou intangíveis, são construídas e definidas dia após dia por meio da gestão e da consistência.</h3>
				</div>
			</article>
			<div class="div-dna-brand-concept">
				<h2>DNA <br>do Brand Concept</h2>
			</div>
			<ul class="brand-concept">
				<li>Prototipagem</li>
				<li>Aplicação</li>
				<li>Analise de Mercado</li>
				<li>Finalização</li>
				<li>Composição de atributos</li>
			</ul>
		</div>
	</section>

	<?php 
		// TEMPLATE BLOG
		include (TEMPLATEPATH . '/templates/blog.php'); 
	?>

	<div class="hand-containerLarge">
		<section class="hand-info-prod">
			<span>Let’s Talk</span>
			<h3>E ai? vamos <br>construir uma <br>marca incrível?🤩</h3>
			<a class="" href="#">Let's do This!</a>
		</section>
	</div>

</div>	