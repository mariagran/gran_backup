<?php 
	$projeto_template_home =  rwmb_meta('projeto_template_home');
	if($projeto_template_home == "annaCuciucs"):
?>
<!-- SESSÃO IMAGEM CELULAR  -->
<section class="hand-oqueFazemos-detalheSocialMedia">
	<div class="hand-container">
		<div class="row">
			<div class="col-sm-12">
				<div class="hand-social-cel">
					<figure>
						<img src="<?php echo rwmb_meta('projeto_Mockup_Celular')['full_url']?>" alt="<?php echo rwmb_meta('projeto_Mockup_Celular')['full_url'];?>">
						<figcaption class="hidden"></figcaption>
					</figure>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="hand-social-cel rigth">
					<figure>
						<img src="<?php echo rwmb_meta('projeto_Mockup_Celular_direito')['full_url']?>" alt="<?php echo rwmb_meta('projeto_Mockup_Celular_direito')['full_url'];?>">
						<figcaption class="hidden"></figcaption>
					</figure>
				</div>
			</div>						
		</div>							
	</div>	
</section>
<?php  elseif($projeto_template_home == "atr"): ?>
	
<!-- SESSÃO IMAGEM CELULAR  -->
<section class="hand-oqueFazemos-detalheSocialMedia">
	<div class="hand-container">
		<div class="row">
			<div class="col-sm-3">
				<div class="hand-social-cel">
					<figure>
						<img src="<?php echo rwmb_meta('projeto_Mockup_Celular')['full_url']?>" alt="<?php echo rwmb_meta('projeto_Mockup_Celular')['full_url'];?>">
						<figcaption class="hidden"></figcaption>
					</figure>
				</div>
			</div>
			<div class="col-sm-9">
				<div class="hand-social-cel rigth">
					<figure>
						<img src="<?php echo rwmb_meta('projeto_Mockup_Celular_direito')['full_url']?>" alt="<?php echo rwmb_meta('projeto_Mockup_Celular_direito')['full_url'];?>">
						<figcaption class="hidden"></figcaption>
					</figure>
				</div>
			</div>						
		</div>							
	</div>	
</section>
<?php  elseif($projeto_template_home == "chicano"): ?>

	<!-- SESSÃO IMAGEM CELULAR  -->
<section class="hand-oqueFazemos-detalheSocialMedia chicano">
	<div class="hand-container">
		<div class="row">
			<div class="col-sm-6">
				<div class="hand-social-cel">
					<figure>
						<img src="<?php echo rwmb_meta('projeto_Mockup_Celular')['full_url']?>" alt="<?php echo rwmb_meta('projeto_Mockup_Celular')['full_url'];?>">
						<figcaption class="hidden"></figcaption>
					</figure>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="hand-social-cel rigth">
					<figure>
						<img src="<?php echo rwmb_meta('projeto_Mockup_Celular_direito')['full_url']?>" alt="<?php echo rwmb_meta('projeto_Mockup_Celular_direito')['full_url'];?>">
						<figcaption class="hidden"></figcaption>
					</figure>
				</div>
			</div>	

			<div class="col-sm-12">
				<div class="hand-social-cel center">
					<figure>
						<img src="<?php echo rwmb_meta('projeto_Mockup_Celular_centro')['full_url']?>" alt="<?php echo rwmb_meta('projeto_Mockup_Celular_centro')['full_url'];?>">
						<figcaption class="hidden"></figcaption>
					</figure>
				</div>
			</div>						
		</div>							
	</div>	
</section>


<?php  elseif($projeto_template_home == "lalomiteria"): ?>

<!-- SESSÃO IMAGEM CELULAR  -->
<section class="hand-oqueFazemos-detalheSocialMedia chicano">
	<div class="hand-container">
		<div class="row">
			<div class="col-sm-4">
				<div class="hand-social-cel">
					<figure>
						<img src="<?php echo rwmb_meta('projeto_Mockup_Celular')['full_url']?>" alt="<?php echo rwmb_meta('projeto_Mockup_Celular')['full_url'];?>">
						<figcaption class="hidden"></figcaption>
					</figure>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="hand-social-cel rigth">
					<figure>
						<img src="<?php echo rwmb_meta('projeto_Mockup_Celular_direito')['full_url']?>" alt="<?php echo rwmb_meta('projeto_Mockup_Celular_direito')['full_url'];?>">
						<figcaption class="hidden"></figcaption>
					</figure>
				</div>
			</div>	

			<div class="col-sm-4">
				<div class="hand-social-cel center">
					<figure>
						<img src="<?php echo rwmb_meta('projeto_Mockup_Celular_centro')['full_url']?>" alt="<?php echo rwmb_meta('projeto_Mockup_Celular_centro')['full_url'];?>">
						<figcaption class="hidden"></figcaption>
					</figure>
				</div>
			</div>						
		</div>							
	</div>	
</section>


<!-- SESSÃO IMAGEM CELULAR  -->
<section class="hand-oqueFazemos-detalheSocialMedia lomiteria">
	<div class="hand-container">
		<div class="row">
			<div class="col-sm-6">
				<div class="hand-social-cel">
					<figure>
						<img src="<?php echo rwmb_meta('projeto_Mockup_Celular_1')['full_url']?>" alt="<?php echo rwmb_meta('projeto_Mockup_Celular_1')['full_url'];?>">
						<figcaption class="hidden"></figcaption>
					</figure>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="hand-social-cel center">
					<figure>
						<img src="<?php echo rwmb_meta('projeto_Mockup_Celular_direito_1')['full_url']?>" alt="<?php echo rwmb_meta('projeto_Mockup_Celular_direito_1')['full_url'];?>">
						<figcaption class="hidden"></figcaption>
					</figure>
				</div>
			</div>						
		</div>							
	</div>	
</section>
<?php  endif; ?>

<!-- SESSÃO IMAGEM CELULAR  -->
<section class="hand-oqueFazemos-detalheSocialMedia">
	
		<div class="row">
			<div class="col-sm-12">
				<div class="hand-social-cel center">
					<figure>
						<img src="<?php echo rwmb_meta('projeto_Mockup_full')['full_url']?>" alt="<?php echo rwmb_meta('projeto_Mockup_full')['full_url'];?>">
						<figcaption class="hidden"></figcaption>
					</figure>
				</div>
			</div>						
		</div>							
	
</section>