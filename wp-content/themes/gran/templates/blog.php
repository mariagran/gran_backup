
<section class="hand-post-blog">
	<h3 class="hidden">Blog</h3>
	
		<ul class="grid-post">
			<?php 
				//LOOP DE POST SERVIÇOS
				$posts = new WP_Query( array( 'post_type' => 'post', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => 3) );
				while ( $posts->have_posts() ) : $posts->the_post();
			 ?>
			<li class="single-post">
				<a href="<?php echo get_permalink() ?>">
					<figure>
						<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
					</figure>
					<span>10 Nov 2019</span>
					<h2><?php echo get_the_title() ?></h2>
					<div class="icones">
						<ul>
							<li>
								<figure>
									<img src="<?php echo get_template_directory_uri(); ?>/img/heart.svg">
								</figure>
								<div class="quantidade">
									<span>1.323</span>
								</div>
							</li>
							<li><figure>
									<img src="<?php echo get_template_directory_uri(); ?>/img/message-square.png">
								</figure>
								<div class="quantidade">
									<span>23</span>
								</div>
							</li>
							<li class="ultimo-item">
								<figure>
									<img src="<?php echo get_template_directory_uri(); ?>/img/bookmark.svg">
								</figure>
							</li>
						</ul>	
						
					</div>
				</a>
			</li>
			<?php endwhile; wp_reset_query(); ?>
		</ul>
</section>
