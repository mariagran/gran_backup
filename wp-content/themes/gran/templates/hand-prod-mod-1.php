<div class="pg hand-prod-mod-1">
					
	<section class="hand-destaque-prod">
		<img src="<?php echo get_template_directory_uri(); ?>/img/grid-fundo.svg">
		<div class="hand-containerLarge">
			<article>
				<span class="hand-subTitle-topo"><?php echo $terms[0]->name; ?></span>
				<h1 class="hand-title-destaque"><?php echo get_the_title(); ?></h1>
				<p class="hand-paragrafo-deatque"><?php echo rwmb_meta('Gran_servico_subtitulo_commerce_Marketing'); ?></p>
			</article>

			<nav>
				<ul id="carrosselMenuItem" class="owl-carousel">
					<?php while($servicos->have_posts()) : $servicos->the_post(); ?>
					<li class="item  <?php if($post_title == get_the_title()){ echo $ativo_item;}; ?> ">
						<a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>
					</li>
					<?php endwhile;wp_reset_query(); ?>
				</ul>
			</nav>
		</div>
	</section>

	<section class="hand-conteudo-prod">
		
		<article>
			<div class="hand-container">
				<div class="row">
					<div class="col-md-4">
						<div class="hand-sub-title">
							<h3 class="hand-title-section-home"><?php echo rwmb_meta('Gran_servico_titulo_commerce_Marketing'); ?></h3>
						</div>
					</div>
					<div class="col-md-8">
						<div class="row">
							<div class="col-sm-6">
								<div class="hand-texto-prod hand-formatacao-texto">
									<?php echo rwmb_meta('Gran_servico_texto_esquerda_commerce_Marketing'); ?>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="hand-texto-prod formatacaoLista">
									<?php echo rwmb_meta('Gran_servico_texto_direita_commerce_Marketing'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</article>

		<div class="hand-containerFull">
			<article>

				<div class="row">
					<div class="col-sm-5">
						<div class="hand-sub-title mobile-title">
							<h3><?php echo rwmb_meta('Gran_servico_texto_box_direita_commerce_Marketing'); ?></h3>
						</div>
						<div class="hand-texto-prod formatacaoLista">
							<?php echo rwmb_meta('Gran_servico_texto_box_esquerda_commerce_Marketing'); ?>
						</div>
					</div>	
					<div class="col-sm-7">
						<div class="hand-sub-title desktop-title">
							<h3><?php echo rwmb_meta('Gran_servico_texto_box_direita_commerce_Marketing'); ?></h3>
						</div>
					</div>	
				</div>
				
			</article>
		</div>	
			
	</section>

	<div class="hand-containerLarge">
		<section class="hand-our-plans-prod">
				
				<div class="hand-title-prod">
					<span><?php echo rwmb_meta('Gran_servico_Lets_work_togeter_commerce_Marketing'); ?></span>
					<h3 class=""><?php echo rwmb_meta('Gran_servico_Our_Plans_commerce_Marketing'); ?></h3>
				</div>

				<div class="row">
					<div class="col-md-4">
						<div class="hand-contant-text-prod hand-formatacao-texto">
							<?php echo rwmb_meta('Gran_servico_duvida_commerce_Marketing'); ?>
						</div>
					</div>
					<div class="col-md-8">
						
						<div class="hand-table-prod">
							
							<div class="hand-table-title-prod">
								<div class="row">
									<div class="col-xs-6  col-sm-3">
										<figure>
											<img src="<?php echo get_template_directory_uri(); ?>/img/briefcase.png">
											<figcaption class="hidden">Logo</figcaption>	
										</figure>
									</div>
									<div class="col-xs-2 col-sm-3">
										<strong>Basic</strong>
									</div>
									<div class="col-xs-2  col-sm-3">
										<strong>Business</strong>
									</div>
									<div class="col-xs-2  col-sm-3">
										<strong>Pro</strong>
									</div>
								</div>
							</div>

							<?php 

								$check_list_commerce_Marketing = rwmb_meta('servico_check_list_commerce_Marketing'); 
								foreach ($check_list_commerce_Marketing as $check_list_commerce_Marketing):
									$nome     = trim($check_list_commerce_Marketing['nome']);
							?>
							<div class="hand-table-content-prod">
								<div class="row">
									<div class="col-xs-6 col-sm-3">
										<h2><?php echo $nome; ?></h2>
									</div>
									<div class="col-xs-2  col-sm-3">
										<figure>
											<?php if ((int)$check_list_commerce_Marketing['basic'] != 0): ?>
											<img src="<?php echo get_template_directory_uri(); ?>/img/tick.svg" alt="<?php echo $nome  ?>">
											<?php endif; ?>
											<figcaption class="hidden"><?php echo $nome  ?></figcaption>	
										</figure>
									</div>
									<div class="col-xs-2 col-sm-3">
										<figure>
											<?php if ((int)$check_list_commerce_Marketing['business'] != 0): ?>
											<img src="<?php echo get_template_directory_uri(); ?>/img/tick.svg" alt="<?php echo $nome  ?>">
											<?php endif; ?>
											<figcaption class="hidden"><?php echo $nome  ?></figcaption>	
										</figure>
									</div>
									<div class="col-xs-2 col-sm-3">
										<figure>
											<?php if ((int)$check_list_commerce_Marketing['pro'] != 0): ?>
											<img src="<?php echo get_template_directory_uri(); ?>/img/tick.svg" alt="<?php echo $nome  ?>">
											<?php endif; ?>
											<figcaption class="hidden"><?php echo $nome  ?></figcaption>	
										</figure>
									</div>
								</div>
							</div>
							<?php endforeach; ?>
						</div>

						<div class="hand-link-prod">
							<a href="#" class="especialista">Fale com um Especialista</a>
							<div class="row">
								<div class="col-xs-6 col-sm-3">
								</div>
								<div class="col-xs-2 col-sm-3">
									<a href="#">Saiba mais</a>
								</div>
								<div class="col-xs-2 col-sm-3">
									<a href="#">Saiba mais</a>
								</div>
								<div class="col-xs-2 col-sm-3">
									<a href="#">Saiba mais</a>
								</div>
							</div>
						</div>

					</div>
				</div>

		</section>
	</div>

	<div class="hand-containerLarge">
		<section class="hand-info-prod">
			<span><?php  echo rwmb_meta('Gran_servico_subtitulo_rodape_commerce_Marketing') ?></span>
			<h3><?php  echo rwmb_meta('Gran_servico_frase_rodape_commerce_Marketing') ?></h3>
			<a class="" href="<?php  echo rwmb_meta('Gran_servico_link_rodape_commerce_Marketing') ?>">Bora</a>
		</section>
	</div>

</div>
	