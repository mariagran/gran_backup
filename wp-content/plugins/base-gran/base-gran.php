<?php

/**
 * Plugin Name: Base We Payout
 * Description: Controle base do tema We Payout.
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: http://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */

    function baseGran () {

		// TIPOS DE CONTEÚDO
		conteudosGran();

		//TAXONOMIA
		taxonomia_Gran();

		//METABOX
		metaboxesGran();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/
	function conteudosGran(){

		//POST TYPE PARCEIROS
		tipoParceiros();

		//POST TYPE SERVIÇOS
		tipoServico();	

		//POST TYPE VAGAS
		tipoVagas();

		// TIPO DE PERGUNTAS
		tipoPerguntasFrequentes();

		// TIPO DE LOGO
		tipoLogoDesignNaming();

		// TIPO DE PROJETOS
		tipoProjetos();

	/****************************************************
	* TIPOS DE CONTEÚDO - English
	*****************************************************/

		//POST TYPE SERVIÇOS
		tipoServico_English();
		
	}
	
	// CUSTOM POST TPE SERVIÇOS OFERECIDOS
	function tipoServico() {

		$rotulosServico = array(
								'name'               => 'Serviços',
								'singular_name'      => 'servico',
								'menu_name'          => 'Serviços',
								'name_admin_bar'     => 'Serviços',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo servico',
								'new_item'           => 'Novo servico',
								'edit_item'          => 'Editar servico',
								'view_item'          => 'Ver servico',
								'all_items'          => 'Todos os servicos',
								'search_items'       => 'Buscar servico',
								'parent_item_colon'  => 'Dos servicos',
								'not_found'          => 'Nenhum servico cadastrado.',
								'not_found_in_trash' => 'Nenhum servico na lixeira.'
							);

		$argsServico 	= array(
								'labels'             => $rotulosServico,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'servicos' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('servico', $argsServico);

	}

	// CUSTOM POST TYPE PARCEIROS
	function tipoParceiros() {

		$rotulosParceiros = array(
								'name'               => 'Parceiros',
								'singular_name'      => 'parceiro',
								'menu_name'          => 'Parceiros',
								'name_admin_bar'     => 'Parceiros',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo parceiro',
								'new_item'           => 'Novo parceiro',
								'edit_item'          => 'Editar parceiro',
								'view_item'          => 'Ver parceiro',
								'all_items'          => 'Todos os parceiros',
								'search_items'       => 'Buscar parceiro',
								'parent_item_colon'  => 'Dos parceiros',
								'not_found'          => 'Nenhum parceiro cadastrado.',
								'not_found_in_trash' => 'Nenhum parceiro na lixeira.'
							);

		$argsParceiros 	= array(
								'labels'             => $rotulosParceiros,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'parceiros' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('parceiro', $argsParceiros);

	}

	// CUSTOM POST TYPE VAGAS
	function tipoVagas() {

		$rotulosVagas = array(
								'name'               => 'Vagas',
								'singular_name'      => 'vaga',
								'menu_name'          => 'Vagas',
								'name_admin_bar'     => 'Vagas',
								'add_new'            => 'Adicionar nova',
								'add_new_item'       => 'Adicionar nova vaga',
								'new_item'           => 'Nova vaga',
								'edit_item'          => 'Editar vaga',
								'view_item'          => 'Ver vaga',
								'all_items'          => 'Todas as vagas',
								'search_items'       => 'Buscar vaga',
								'parent_item_colon'  => 'Das vagas',
								'not_found'          => 'Nenhuma vaga cadastrado.',
								'not_found_in_trash' => 'Nenhuma vaga na lixeira.'
							);

		$argsVagas 	= array(
								'labels'             => $rotulosVagas,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'vagas' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail','editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('vaga', $argsVagas);

	}

	// CUSTOM POST TYPE PERGUNTAS FREQUENTES
	function tipoPerguntasFrequentes() {

		$rotulosPerguntasFrequentes = array(
								'name'               => 'Perguntas frequentes',
								'singular_name'      => 'perguntas_frequentes',
								'menu_name'          => 'Perguntas frequentes',
								'name_admin_bar'     => 'Perguntas frequentes',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar nova pergunta',
								'new_item'           => 'Nova pergunta',
								'edit_item'          => 'Editar pergunta',
								'view_item'          => 'Ver pergunta',
								'all_items'          => 'Todas as perguntas',
								'search_items'       => 'Buscar pergunta frequente',
								'parent_item_colon'  => 'Das perguntas frequentes',
								'not_found'          => 'Nenhuma pergunta cadastrado.',
								'not_found_in_trash' => 'Nenhuma pergunta na lixeira.'
							);

		$argsPerguntasFrequentes 	= array(
								'labels'             => $rotulosPerguntasFrequentes,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'perguntas_frequentes' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('perguntas_frequentes', $argsPerguntasFrequentes);

	}

	// CUSTOM POST TYPE PERGUNTAS FREQUENTES
	function tipoLogoDesignNaming() {

		$rotulosLogoDesignNaming = array(
								'name'               => 'Logo Design Naming',
								'singular_name'      => 'Logo Design Naming',
								'menu_name'          => 'Logos Design Naming',
								'name_admin_bar'     => 'Logos Design Naming',
								'add_new'            => 'Adicionar nova',
								'add_new_item'       => 'Adicionar nova logo',
								'new_item'           => 'Nova pergunta',
								'edit_item'          => 'Editar logo',
								'view_item'          => 'Ver logo',
								'all_items'          => 'Todas as logos',
								'search_items'       => 'Buscar logo',
								'parent_item_colon'  => 'Das logos',
								'not_found'          => 'Nenhuma logo cadastrado.',
								'not_found_in_trash' => 'Nenhuma logo na lixeira.'
							);

		$argsLogoDesignNaming 	= array(
								'labels'             => $rotulosLogoDesignNaming,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'logo-design-naming' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array('title','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('logodesignnaming', $argsLogoDesignNaming);

	}

	// CUSTOM POST TYPE PROJETOS
	function tipoProjetos() {

		$rotulosProjetos = array(
								'name'               => 'Projeto',
								'singular_name'      => 'Projeto',
								'menu_name'          => 'Projetos',
								'name_admin_bar'     => 'Projeto',
								'add_new'            => 'Adicionar novo projeto',
								'add_new_item'       => 'Adicionar novo projeto',
								'new_item'           => 'Novo pergunta',
								'edit_item'          => 'Editar projeto',
								'view_item'          => 'Ver projeto',
								'all_items'          => 'Todas as projetos',
								'search_items'       => 'Buscar projeto',
								'parent_item_colon'  => 'Dos projetos',
								'not_found'          => 'Nenhum projeto cadastrado.',
								'not_found_in_trash' => 'Nenhum projeto na lixeira.'
							);

		$argsProjetos 	= array(
								'labels'             => $rotulosProjetos,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'projetos' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array('title','excerpt','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('projetos', $argsProjetos);

	}


	// CUSTOM POST TPE SERVIÇOS OFERECIDOS -English
	function tipoServico_English() {

		$rotulosServico_english = array(
								'name'               => 'Services',
								'singular_name'      => 'service',
								'menu_name'          => 'Services',
								'name_admin_bar'     => 'Services',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo service',
								'new_item'           => 'Novo service',
								'edit_item'          => 'Editar service',
								'view_item'          => 'Ver service',
								'all_items'          => 'Todos os services',
								'search_items'       => 'Buscar service',
								'parent_item_colon'  => 'Dos services',
								'not_found'          => 'Nenhum service cadastrado.',
								'not_found_in_trash' => 'Nenhum service na lixeira.'
							);

		$argsServico_english 	= array(
								'labels'             => $rotulosServico_english,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'services' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('servico_english', $argsServico_english);

	}

	/****************************************************
	* TAXONOMIA
	*****************************************************/
		
		function taxonomia_Gran () {

			taxonomiaServicos();

			taxonomiaLogoDesignNaming();

			/****************************************************
			* TAXONOMIA - English
			*****************************************************/

			taxonomiaServicos_english();
			

		}

		function taxonomiaServicos() {

			$rotulosCategoriaServicos = array(

											'name'              => 'Categorias de serviço',
											'singular_name'     => 'Categorias de serviços',
											'search_items'      => 'Buscar categoria do serviço',
											'all_items'         => 'Todas as categorias',
											'parent_item'       => 'Categoria pai',
											'parent_item_colon' => 'Categoria pai:',
											'edit_item'         => 'Editar categoria do serviço',
											'update_item'       => 'Atualizar categoria',
											'add_new_item'      => 'Nova categoria',
											'new_item_name'     => 'Nova categoria',
											'menu_name'         => 'Categorias serviço',
										);

			$argsCategoriaServicos 	= array(

											'hierarchical'      => true,
											'labels'            => $rotulosCategoriaServicos,
											'show_ui'           => true,
											'show_admin_column' => true,
											'query_var'         => true,
											'rewrite'           => array( 'slug' => 'categoria-servico' ),
										);

			register_taxonomy( 'categoriaservicos', array( 'servico' ), $argsCategoriaServicos);
		}

		function taxonomiaServicos_english() {

			$rotulosCategoriaServicos_english = array(

											'name'              => 'Categorias de serviço',
											'singular_name'     => 'Categorias de serviços',
											'search_items'      => 'Buscar categoria do serviço',
											'all_items'         => 'Todas as categorias',
											'parent_item'       => 'Categoria pai',
											'parent_item_colon' => 'Categoria pai:',
											'edit_item'         => 'Editar categoria do serviço',
											'update_item'       => 'Atualizar categoria',
											'add_new_item'      => 'Nova categoria',
											'new_item_name'     => 'Nova categoria',
											'menu_name'         => 'Categorias serviço',
										);

			$argsCategoriaServicos_english 	= array(

											'hierarchical'      => true,
											'labels'            => $rotulosCategoriaServicos_english,
											'show_ui'           => true,
											'show_admin_column' => true,
											'query_var'         => true,
											'rewrite'           => array( 'slug' => 'services-category' ),
										);

			register_taxonomy( 'categoriaservicos_english', array( 'servico_english' ), $argsCategoriaServicos_english);
		}

		function taxonomiaLogoDesignNaming() {

			$rotulosCategoriaLogoDesignNaming = array(

											'name'              => 'Categorias',
											'singular_name'     => 'Categorias',
											'search_items'      => 'Buscar categoria',
											'all_items'         => 'Todas as categorias',
											'parent_item'       => 'Categoria pai',
											'parent_item_colon' => 'Categoria pai:',
											'edit_item'         => 'Editar categoria',
											'update_item'       => 'Atualizar categoria',
											'add_new_item'      => 'Nova categoria',
											'new_item_name'     => 'Nova categoria',
											'menu_name'         => 'Categorias',
										);

			$argsCategoriaLogoDesignNaming 	= array(

											'hierarchical'      => true,
											'labels'            => $rotulosCategoriaLogoDesignNaming,
											'show_ui'           => true,
											'show_admin_column' => true,
											'query_var'         => true,
											'rewrite'           => array( 'slug' => 'categoria-logo' ),
										);

			register_taxonomy( 'categorialogodesignnaming', array( 'logodesignnaming' ), $argsCategoriaLogoDesignNaming);
		}

	/****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesGran(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}
		function registraMetaboxes( $metaboxes ){

			$prefix = 'Gran_';

			// METABOX DE E-commerce Marketing
			$metaboxes[] = array(

				'id'			=> 'detalhes_metabox_servico_e_commerce_Marketing',
				'title'			=> 'Detalhes E-commerce Marketing',
				'pages' 		=> array( 'servico' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Frase Subtítulo: ',
						'id'    => "{$prefix}servico_subtitulo_commerce_Marketing",
						'desc'  => 'Frase Yes, we are online and now what?',
						'type'  => 'text'
					),	
					array(
						'name'  => 'Título descrição: ',
						'id'    => "{$prefix}servico_titulo_commerce_Marketing",
						'desc'  => 'Frase Yes, we are online and now what?',
						'type'  => 'textarea'
					),
					array(
						'name'  => 'Texto descritivo esquerda: ',
						'id'    => "{$prefix}servico_texto_esquerda_commerce_Marketing",
						'type'  => 'wysiwyg'
					),	
					array(
						'name'  => 'Texto descritivo direita: ',
						'id'    => "{$prefix}servico_texto_direita_commerce_Marketing",
						'type'  => 'wysiwyg'
					),	
					array(
						'name'  => 'Texto descritivo box escuro esquerda: ',
						'id'    => "{$prefix}servico_texto_box_esquerda_commerce_Marketing",
						'type'  => 'wysiwyg'
					),	
					array(
						'name'  => 'Fease descritivo box escuro direita: ',
						'id'    => "{$prefix}servico_texto_box_direita_commerce_Marketing",
						'type'  => 'textarea'
					),
					array(
						'name'  => 'Let’s work togeter: ',
						'id'    => "{$prefix}servico_Lets_work_togeter_commerce_Marketing",
						'type'  => 'text'
					),
					array(
						'name'  => 'Our Plans: ',
						'id'    => "{$prefix}servico_Our_Plans_commerce_Marketing",
						'type'  => 'text'
					),
					array(
						'name'  => 'Está na dúvida?: ',
						'id'    => "{$prefix}servico_duvida_commerce_Marketing",
						'type'  => 'wysiwyg'
					),	
					array(
					    'name'    => 'Check List Serviços:',
					    'id'      => 'servico_check_list_commerce_Marketing',
					    'type'    => 'fieldset_text',

					    'options' => array(
					        'nome'      => 'Nome',
					        'basic'     => 'Basic',
					        'business'  => 'Business',
					        'pro'       => 'Pro',
					    ),
					    // Is field cloneable?
					    'clone' => true,
					    'desc'  => "Marque 1 para dar check",
					),
					array(
						'name'  => 'Frase subtítulo rodapé - Let’s Talk: ',
						'id'    => "{$prefix}servico_subtitulo_rodape_commerce_Marketing",
						'type'  => 'text'
					),	
					array(
						'name'  => 'Frase  rodapé - So, let’s Rock?: ',
						'id'    => "{$prefix}servico_frase_rodape_commerce_Marketing",
						'type'  => 'textarea'
					),	
					array(
						'name'  => 'Frase  rodapé - Link: ',
						'id'    => "{$prefix}servico_link_rodape_commerce_Marketing",
						'type'  => 'text'
					),	
				),
			);

			// METABOX DE E-commerce Marketing
			$metaboxes[] = array(

				'id'			=> 'detalhes_metabox_servico_Inbound_Marketing',
				'title'			=> 'Detalhes Inbound Marketing',
				'pages' 		=> array( 'servico' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
					    'type' => 'text',
					    'name' => 'Título - O que é Inbound marketing',
					    'id'   => 'servico_titulo_im_Inbound_Marketing',
					    'desc'   => 'O que é Inbound marketing',
					),
					array(
					    'type' => 'wysiwyg',
					    'name' => 'Texto - O que é Inbound marketing',
					    'id'   => 'servico_texto_im_Inbound_Marketing',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Icone box escuro - esquerdo',
					    'id'   => 'servico_icone_box_esquerdo_Inbound_Marketing',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Icone box escuro - direito',
					    'id'   => 'servico_icone_box_direito_Inbound_Marketing',
					),
					array(
					    'type' => 'textarea',
					    'name' => 'Texto box escuro - esquerdo',
					    'id'   => 'servico_texto_box_esquerdo_Inbound_Marketing',
					),
					array(
					    'type' => 'textarea',
					    'name' => 'Texto box escuro - direito',
					    'id'   => 'servico_texto_box_direito_Inbound_Marketing',
					),
					array(
					    'type' => 'text',
					    'name' => 'Título Plano tático',
					    'id'   => 'servico_titulo_plano_tatico_Inbound_Marketing',
					),
					array(
					    'type' => 'text',
					    'name' => 'SubTítulo Plano tático',
					    'id'   => 'servico_Subtitulo_plano_tatico_Inbound_Marketing',
					),
					array(
					    'name'    => 'Check List Plano Tático:',
					    'id'      => 'servico_lis_plano_tatico_Inbound_Marketing',
					    'type'    => 'fieldset_text',

					    'options' => array(
					        'titulo'      => 'Título',
					        'basic'       => 'Texto',
					    ),
					    // Is field cloneable?
					    'clone' => true,
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Foto ilustrativa - Plano Tático',
					    'id'   => 'servico_ilustrativo_plano_tatico_Inbound_Marketing',
					),
					array(
					    'type' => 'wysiwyg',
					    'name' => 'Título - Como funciona',
					    'id'   => 'servico_titulo_como_funciona_Inbound_Marketing',
					),
					array(
					    'type' => 'textarea',
					    'name' => 'Texto - Como funciona',
					    'id'   => 'servico_texto_como_funciona_Inbound_Marketing',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Foto ilustrativa - Como funciona',
					    'id'   => 'servico_ilustrativo_como_funciona_Inbound_Marketing',
					),
					array(
					    'name'    => 'List Plano Como funciona:',
					    'id'      => 'servico_list_como_funciona_Inbound_Marketing',
					    'type'    => 'fieldset_text',

					    'options' => array(
					        'titulo'      => 'Título',
					        'texto'       => 'Texto',
					    ),
					    // Is field cloneable?
					    'clone' => true,
					     'max_clone' => '6',

					),
					array(
					    'name'    => 'List ícone:',
					    'id'      => 'servico_list_icone_como_funciona_Inbound_Marketing',
					    'type'    => 'single_image',

					    'options' => array(
					        'titulo'      => 'Título',
					    ),
					    // Is field cloneable?
					    'clone'     => true,
					    'max_clone' => '6',
					),
					array(
					    'type' => 'text',
					    'name' => 'Título css',
					    'id'   => 'servico_titulo_360_Inbound_Marketing',
					),
					array(
					    'type' => 'textarea',
					    'name' => 'Texto 360°',
					    'id'   => 'servico_texto_360_Inbound_Marketing',
					),
					array(
					    'type' => 'text',
					    'name' => 'Lista 360°',
					    'id'   => 'servico_list_360_Inbound_Marketing',
					    'clone'     => true,

					),
					array(
					    'type' => 'single_image',
					    'name' => 'Icone canais estratégia',
					    'id'   => 'servico_icone_canais_estrategia_Inbound_Marketing',
					),
					array(
					    'type' => 'wysiwyg',
					    'name' => 'Texto canais estratégia',
					    'id'   => 'servico_texto_canais_estrategia_Inbound_Marketing',
					),
					array(
					    'type' => 'text',
					    'name' => 'Título - Qualquer negócio',
					    'id'   => 'servico_titulo_qualquer_negocio_Inbound_Marketing',
					),
					array(
					    'type' => 'wysiwyg',
					    'name' => 'Texto - Qualquer negócio esquerda',
					    'id'   => 'servico_texto_qualquer_negocio_esquerdo_Inbound_Marketing',
					),
					array(
					    'type' => 'wysiwyg',
					    'name' => 'Texto - Qualquer negócio direita',
					    'id'   => 'servico_texto_qualquer_negocio_direita_Inbound_Marketing',
					),
					array(
					    'type' => 'text',
					    'name' => 'Frase título rodapé',
					    'id'   => 'servico_frase_titulo_rodape_Inbound_Marketing',
					),
					array(
					    'type' => 'wysiwyg',
					    'name' => 'Texto título rodapé',
					    'id'   => 'servico_frase_texto_rodape_Inbound_Marketing',
					),
				),
			);

			// METABOX DE Social Media Marketing
			$metaboxes[] = array(

				'id'			=> 'detalhes_metabox_social_Media_Marketing',
				'title'			=> 'Detalhes Social Media Marketing',
				'pages' 		=> array( 'servico' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Frase Subtítulo: ',
						'id'    => "{$prefix}servico_subtitulo_social_Media_Marketing",
						'desc'  => 'Frase Yes, we are online and now what?',
						'type'  => 'text'
					),

					array(
						'name'  => 'Texto Social Media: ',
						'id'    => "{$prefix}servico_texto_social_Media_Marketing",
						'desc'  => 'Texto lado esquerdo',
						'type'  => 'wysiwyg'
					),
					array(
						'name'  => 'Foto Social Media: ',
						'id'    => "{$prefix}servico_foto_social_Media_Marketing",
						'type'  => 'single_image'
					),
					array(
						'name'  => 'Foto Social Media - SMM : ',
						'id'    => "{$prefix}servico_foto_social_Media_Marketing_SMM",
						'desc'  => 'Foto facebook esquerda',
						'type'  => 'single_image'
					),
					array(
						'name'  => 'Social Media - SMM Título : ',
						'id'    => "{$prefix}servico_foto_social_Media_Marketing_SMM_titulo",
						'desc'  => 'Título facebook esquerda',
						'type'  => 'text'
					),
					array(
						'name'  => 'Social Media - SMM Texto : ',
						'id'    => "{$prefix}servico_foto_social_Media_Marketing_SMM_texto",
						'desc'  => 'Texto facebook esquerda',
						'type'  => 'wysiwyg'
					),
					array(
						'name'  => 'Título Box - Facebook ADS : ',
						'id'    => "{$prefix}servico_foto_social_Media_Marketing_box_titulo",
						'desc'  => 'Título box',
						'type'  => 'text'
					),
					array(
						'name'  => 'Texto Box - Facebook ADS : ',
						'id'    => "{$prefix}servico_foto_social_Media_Marketing_box_texto",
						'desc'  => 'Texto box',
						'type'  => 'textarea'
					),
					array(
						'name'  => 'Título Instagram - Instagram ADS : ',
						'id'    => "{$prefix}servico_foto_social_Media_Marketing_Instagram_titulo",
						'desc'  => 'Título Instagram',
						'type'  => 'text'
					),
					array(
						'name'  => 'Texto  - Instagram ADS : ',
						'id'    => "{$prefix}servico_foto_social_Media_Marketing_Instagram_texto",
						'desc'  => 'Texto Instagram',
						'type'  => 'textarea'
					),
					array(
						'name'  => 'Foto Instagram Social Media: ',
						'id'    => "{$prefix}servico_foto_social_Media_Marketing_Instagram_foto",
						'type'  => 'single_image'
					),
					array(
						'name'  => 'Frase Rodapé: ',
						'id'    => "{$prefix}servico_foto_social_Media_Marketing_frase_odape",
						'type'  => 'textarea'
					),
					array(
						'name'  => 'Link texto Rodapé: ',
						'id'    => "{$prefix}servico_foto_social_Media_Marketing_frase_link_texto",
						'type'  => 'text'
					),
					array(
						'name'  => 'Link Rodapé: ',
						'id'    => "{$prefix}servico_foto_social_Media_Marketing_frase_link",
						'type'  => 'text'
					),
				),
			);


			// METABOX DE PAID SEARCH
			$metaboxes[] = array(

				'id'			=> 'detalhes_metabox_paid_search',
				'title'			=> 'Detalhes Paid Search',
				'pages' 		=> array( 'servico' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
					    'type' => 'text',
					    'name' => 'Subtítulo',
					    'id'   => "{$prefix}servico_subtitulo_paid_search",
					    'desc'   => 'Adicione um <br> paa quebra de linha',
					),
					array(
					    'type' => 'text',
					    'name' => 'Título o que é?',
					    'id'   => "{$prefix}servico_paid_search_titulo_oque_e",
					    'desc'   => 'Adicione um <br> paa quebra de linha',
					),

					array(
					    'name' => 'Texto o que é?',
					    'id'   => "{$prefix}servico_paid_search_texto_oque_e",
					    'type' => 'wysiwyg',
					),
					array(
					    'name' => 'Texto lado direito',
					    'id'   => "{$prefix}servico_paid_search_texto_direito",
					    'type' => 'text',
					),
					array(
					    'name'             => 'Logo lado direito',
					    'id'               => "{$prefix}servico_paid_search_logo_direito",
					    'type'             => 'image_advanced',
					    'max_file_uploads' => 4,
					),
					array(
					    'name' => 'Título - benefícios',
					    'id'   => "{$prefix}servico_paid_search_titulo_beneficios_direito",
					    'type' => 'text',
					),
					array(
					    'name'  => 'Lista de benefícios',
					    'id'    => "{$prefix}servico_paid_search_lista_beneficios_direito",
					    'type'  => 'text',
					    'clone' => true,
					),
					array(
					    'name'             => 'imagem ilustrativa',
					    'id'               => "{$prefix}servico_paid_search_img_ilustrativa",
					    'type'             => 'image_advanced',
					    'max_file_uploads' => 1,
					),
					array(
					    'name'             => 'Como funciona?',
					    'id'               => "{$prefix}servico_paid_search_texto_como_funciona",
					    'type'             => 'wysiwyg',
					),
					array(
					    'name'             => 'Ciclo compra',
					    'id'               => "{$prefix}servico_paid_search_titulo_ciclo",
					    'type'             => 'textarea',
					),
					
					array(
					    'id'      => 'servico_paid_search_list_ciclo',
					    'name'    => 'Lista ciclo',
					    'type'    => 'fieldset_text',

					    // Options: array of key => Label for text boxes
					    // Note: key is used as key of array of values stored in the database
					    'options' => array(
					        'titulo'    => 'Titulo',
					        'texto'     => 'Texto',
					    ),



					    // Is field cloneable?
					    'clone' => true,
					),
					array(
					    'name'             => 'How It Works? - Título',
					    'id'               => "{$prefix}servico_paid_search_titulo_How_It_Works",
					    'type'             => 'text',
					),
					array(
					    'name'             => 'How It Works? - Texto',
					    'id'               => "{$prefix}servico_paid_search_texto_How_It_Works",
					    'type'             => 'textarea',
					),
					array(
					    'name'             => 'Ciclo PPC Handgran - Título',
					    'id'               => "{$prefix}servico_paid_search_titulo_Ciclo_PPC",
					    'type'             => 'text',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Planejamento - Imagem',
					    'id'   => 'servico_paid_search_img_Ciclo_Planejamento',
					),
					array(
					    'name'             => 'Planejamento - Título',
					    'id'               => "{$prefix}servico_paid_search_titulo_Ciclo_Planejamento",
					    'type'             => 'text',
					),
					array(
					    'name'             => 'Planejamento - Lista',
					    'id'               => "{$prefix}servico_paid_search_lista_Ciclo_Planejamento",
					    'type'             => 'text',
					    'clone'             => true,
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Criação - Imagem',
					    'id'   => 'servico_paid_search_img_Ciclo_Criacao',
					),
					array(
					    'name'             => 'Criação - Título',
					    'id'               => "{$prefix}servico_paid_search_titulo_Ciclo_Criacao",
					    'type'             => 'text',
					),
					array(
					    'name'             => 'Criação - Lista',
					    'id'               => "{$prefix}servico_paid_search_lista_Ciclo_Criacao",
					    'type'             => 'text',
					    'clone'             => true,
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Otimização - Imagem',
					    'id'   => 'servico_paid_search_img_Ciclo_Otimizacao',
					),
					array(
					    'name'             => 'Otimização - Título',
					    'id'               => "{$prefix}servico_paid_search_titulo_Ciclo_Otimizacao",
					    'type'             => 'text',
					),
					array(
					    'name'             => 'Otimização - Lista',
					    'id'               => "{$prefix}servico_paid_search_lista_Ciclo_Otimizacao",
					    'type'             => 'text',
					    'clone'             => true,
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Mensuração - Imagem',
					    'id'   => 'servico_paid_search_img_Ciclo_Mensuracao',
					),
					array(
					    'name'             => 'Mensuração - Título',
					    'id'               => "{$prefix}servico_paid_search_titulo_Ciclo_Mensuracao",
					    'type'             => 'text',
					),
					array(
					    'name'             => 'Mensuração - Lista',
					    'id'               => "{$prefix}servico_paid_search_lista_Ciclo_Mensuracao",
					    'type'             => 'text',
					    'clone'             => true,
					),
					array(
					    'name'             => 'Our goals for your PPC',
					    'id'               => "{$prefix}servico_paid_search_Ourgoals_titulo",
					    'type'             => 'wysiwyg',
					),
					array(
					    'name'             => 'Our goals for your PPC Lista',
					    'id'               => "{$prefix}servico_paid_search_Ourgoals_lista",
					    'type'             => 'text',
					    'clone'            => true,
					),
					array(
					    'name'             => 'Título Rodapé',
					    'id'               => "{$prefix}servico_paid_search_titulo_rodape",
					    'type'             => 'text',
					),
					array(
					    'name'             => 'Texto Rodapé',
					    'id'               => "{$prefix}servico_paid_search_texto_rodape",
					    'type'             => 'textarea',
					),
					array(
					    'name'             => 'Link Rodapé',
					    'id'               => "{$prefix}servico_paid_search_link_rodape",
					    'type'             => 'text',
					),

				),
			);

			// METABOX DE VAGAS
			$metaboxes[] = array(

				'id'			=> 'detalhes_metabox_design_naming',
				'title'			=> 'Detalhes Design Naming',
				'pages' 		=> array( 'servico' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
					    'type' => 'text',
					    'name' => 'Subtitulo',
					    'id'   => "{$prefix}servico_design_naming_subtitulo",
					    'desc'   => '',
					),
					array(
					    'name' => 'Imagem ilustrativa',
					    'id'   => "{$prefix}servico_design_naming_Imagem_ilustrativa_texto",
					    'type' => 'single_image',
					),
					array(
					    'name' => 'Design Naming -  o que é?',
					    'id'   => "{$prefix}servico_design_naming_texto",
					    'type' => 'wysiwyg',
					),
					array(
					    'name' => 'Imagem ilustrativa - Box Cinza',
					    'id'   => "{$prefix}servico_design_naming_Imagem_ilustrativa_box",
					    'type' => 'single_image',
					),
					array(
					    'name' => 'Título - Box Cinza',
					    'id'   => "{$prefix}servico_design_naming_titulo_box",
					    'type' => 'text',
					),
					array(
					    'name' => 'Texto - Box Cinza',
					    'id'   => "{$prefix}servico_design_naming_texto_box",
					    'type' => 'textarea',
					),
					array(
					    'name' => 'Título - área logo',
					    'id'   => "{$prefix}servico_design_naming_titulo_area_logo",
					    'type' => 'text',
					),
					array(
					    'name' => 'Texto - área logo',
					    'id'   => "{$prefix}servico_design_naming_texto_area_logo",
					    'type' => 'text',
					),
					array(
					    'name' => 'Título Rodapé',
					    'id'   => "{$prefix}servico_design_naming_titulo_rodape",
					    'type' => 'text',
					),
					array(
					    'name' => 'Texto Rodapé',
					    'id'   => "{$prefix}servico_design_naming_texto_rodape",
					    'type' => 'textarea',
					),
					array(
					    'name' => 'Link Rodapé',
					    'id'   => "{$prefix}servico_design_naming_link_rodape",
					    'type' => 'text',
					),
				),
			);
    	    
    	    // METABOX DE VAGAS
			$metaboxes[] = array(

				'id'			=> 'detalhes_metabox_vaga_gran',
				'title'			=> 'Detalhes das Vagas Gran',
				'pages' 		=> array( 'vaga' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
					    'type' => 'text',
					    'name' => 'Breve detalhe da vaga',
					    'id'   => 'vaga_breve_detalhe',
					    'desc'   => 'Breve detalhe da Vaga',
					),
					array(
					    'type' => 'textarea',
					    'name' => 'Descricao da Vaga',
					    'id'   => 'vaga_descricao',
					    'desc'   => 'Mais detalhes da vaga',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Foto da Vaga',
					    'id'   => 'vaga_descricao_foto',
					    'desc' => 'Foto vaga',
					),
					
				),
			);

			// METABOX DE PROJETOS
			$metaboxes[] = array(

				'id'			=> 'detalhes_metabox_projeto',
				'title'			=> 'Detalhes do Projeto',
				'pages' 		=> array( 'projetos' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
					    'name'            => 'Select',
					    'id'              => 'projeto_template_home',
					    'type'            => 'select',
					    // Array of 'value' => 'Label' pairs
					    'options'         => array(
					        'annaCuciucs' => 'Anna Cuciucs',
					        'atr'         => 'ATR',
					        'chicano'     => 'Chicano',
					        'lalomiteria' => 'La Lomiteria',
					       
					    ),
					    // Allow to select multiple value?
					    'multiple'        => false,
					    // Placeholder text
					    'placeholder'     => 'Template',
					    // Display "Select All / None" button?
					    'select_all_none' => true,
					),
					array(
					    'type' => 'textarea',
					    'name' => 'Descrição Home',
					    'id'   => 'projeto_descricao_home',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Projeto Backgound',
					    'id'   => 'projeto_background',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Logo',
					    'id'   => 'projeto_logo',
					    'desc' => 'Projeto Logo',
					),
					array(
					    'type' => 'textarea',
					    'name' => 'Projeto breve descrição',
					    'id'   => 'projeto_breve_descricao',
					    'desc' => 'Breve Descrição do Projeto',
					),

					array(
					    'type' => 'text',
					    'name' => 'Título - O que fazemos',
					    'id'   => 'projeto_titulo_Que_fazemos',
					    'desc' => 'Título o que fazemos',
					),

					array(
					    'type' => 'wysiwyg',
					    'name' => 'Texto - O que fazemos',
					    'id'   => 'projeto_texto_fazemos',
					    'desc' => 'Texto o que fazemos',
					),

					array(
					    'type' => 'text',
					    'name' => 'Título - Serviços realizados',
					    'id'   => 'projeto_titulo_servicos_realizados',
					),
					array(
					    'type'  => 'text',
					    'clone' => true,
					    'name'  => 'Lista - Serviços realizados',
					    'id'    => 'projeto_lista_servicos_realizados',
					),
					array(
					    'name'             => 'Galeria de imagens',
					    'id'               => "projeto_lista_servicos_galeria",
					    'type'             => 'image_advanced',
					    'max_file_uploads' => 10,
					),
					array(
					    'type'  => 'text',
					    'name'  => 'Título - Social Media Marketing',
					    'id'    => 'projeto_Titulo_Social_Media_Marketing',
					),
					array(
					    'type'  => 'textarea',
					    'name'  => 'Texto Social Media Marketing',
					    'id'    => 'projeto_Texto_Social_Media_Marketing',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Mockup 1',
					    'id'   => 'projeto_Mockup_Celular',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Mockup 2',
					    'id'   => 'projeto_Mockup_Celular_direito',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Mockup 3',
					    'id'   => 'projeto_Mockup_Celular_centro',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Mockup 1 Lomiteria',
					    'id'   => 'projeto_Mockup_Celular_1',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Mockup 2 Lomiteria',
					    'id'   => 'projeto_Mockup_Celular_direito_1',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Mockup full',
					    'id'   => 'projeto_Mockup_full',
					),
					array(
					    'type' => 'text',
					    'name' => 'Depoimento - Nome',
					    'id'   => 'projeto_depoimento_nome',
					),
					
					array(
					    'type' => 'textarea',
					    'name' => 'Depoimento - Texto',
					    'id'   => 'projeto_depoimento_texto',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Mockup Depoimento',
					    'id'   => 'projeto_Mockup_depoimento',
					),
					array(
					    'type'  => 'text',
					    'name'  => 'Título - Design De Interface',
					    'id'    => 'projeto_Design_De_Interface',
					),
					array(
					    'type'  => 'textarea',
					    'name'  => 'Texto Design De Interface',
					    'id'    => 'projeto_Texto_Design_De_Interface',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Mockup Desktop',
					    'id'   => 'projeto_Mockup_Desktop',
					),
					
				),
			);



			/****************************************************
			* META BOXES services
			*****************************************************/
			// METABOX DE E-commerce Marketing
			$metaboxes[] = array(

				'id'			=> 'detalhes_metabox_services_e_commerce_Marketing',
				'title'			=> 'Detalhes E-commerce Marketing',
				'pages' 		=> array( 'servico_english' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Frase Subtítulo: ',
						'id'    => "{$prefix}services_english_subtitulo_commerce_Marketing",
						'desc'  => 'Frase Yes, we are online and now what?',
						'type'  => 'text'
					),	
					array(
						'name'  => 'Título descrição: ',
						'id'    => "{$prefix}services_english_titulo_commerce_Marketing",
						'desc'  => 'Frase Yes, we are online and now what?',
						'type'  => 'textarea'
					),
					array(
						'name'  => 'Texto descritivo esquerda: ',
						'id'    => "{$prefix}services_english_texto_esquerda_commerce_Marketing",
						'type'  => 'wysiwyg'
					),	
					array(
						'name'  => 'Texto descritivo direita: ',
						'id'    => "{$prefix}services_english_texto_direita_commerce_Marketing",
						'type'  => 'wysiwyg'
					),	
					array(
						'name'  => 'Texto descritivo box escuro esquerda: ',
						'id'    => "{$prefix}services_english_texto_box_esquerda_commerce_Marketing",
						'type'  => 'wysiwyg'
					),	
					array(
						'name'  => 'Fease descritivo box escuro direita: ',
						'id'    => "{$prefix}services_english_texto_box_direita_commerce_Marketing",
						'type'  => 'textarea'
					),
					array(
						'name'  => 'Let’s work togeter: ',
						'id'    => "{$prefix}services_english_Lets_work_togeter_commerce_Marketing",
						'type'  => 'text'
					),
					array(
						'name'  => 'Our Plans: ',
						'id'    => "{$prefix}services_english_Our_Plans_commerce_Marketing",
						'type'  => 'text'
					),
					array(
						'name'  => 'Está na dúvida?: ',
						'id'    => "{$prefix}services_english_duvida_commerce_Marketing",
						'type'  => 'wysiwyg'
					),	
					array(
					    'name'    => 'Check List Serviços:',
					    'id'      => 'services_english_check_list_commerce_Marketing',
					    'type'    => 'fieldset_text',

					    'options' => array(
					        'nome'      => 'Nome',
					        'basic'     => 'Basic',
					        'business'  => 'Business',
					        'pro'       => 'Pro',
					    ),
					    // Is field cloneable?
					    'clone' => true,
					    'desc'  => "Marque 1 para dar check",
					),
					array(
						'name'  => 'Frase subtítulo rodapé - Let’s Talk: ',
						'id'    => "{$prefix}services_english_subtitulo_rodape_commerce_Marketing",
						'type'  => 'text'
					),	
					array(
						'name'  => 'Frase  rodapé - So, let’s Rock?: ',
						'id'    => "{$prefix}services_english_frase_rodape_commerce_Marketing",
						'type'  => 'textarea'
					),	
					array(
						'name'  => 'Frase  rodapé - Link: ',
						'id'    => "{$prefix}services_english_link_rodape_commerce_Marketing",
						'type'  => 'text'
					),	
				),
			);

			// METABOX DE E-commerce Marketing
			$metaboxes[] = array(

				'id'			=> 'detalhes_metabox_services_english_Inbound_Marketing',
				'title'			=> 'Detalhes Inbound Marketing',
				'pages' 		=> array( 'servico_english' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
					    'type' => 'text',
					    'name' => 'Título - O que é Inbound marketing',
					    'id'   => 'services_english_titulo_im_Inbound_Marketing',
					    'desc'   => 'O que é Inbound marketing',
					),
					array(
					    'type' => 'wysiwyg',
					    'name' => 'Texto - O que é Inbound marketing',
					    'id'   => 'services_english_texto_im_Inbound_Marketing',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Icone box escuro - esquerdo',
					    'id'   => 'services_english_icone_box_esquerdo_Inbound_Marketing',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Icone box escuro - direito',
					    'id'   => 'services_english_icone_box_direito_Inbound_Marketing',
					),
					array(
					    'type' => 'textarea',
					    'name' => 'Texto box escuro - esquerdo',
					    'id'   => 'services_english_texto_box_esquerdo_Inbound_Marketing',
					),
					array(
					    'type' => 'textarea',
					    'name' => 'Texto box escuro - direito',
					    'id'   => 'services_english_texto_box_direito_Inbound_Marketing',
					),
					array(
					    'type' => 'text',
					    'name' => 'Título Plano tático',
					    'id'   => 'services_english_titulo_plano_tatico_Inbound_Marketing',
					),
					array(
					    'type' => 'text',
					    'name' => 'SubTítulo Plano tático',
					    'id'   => 'services_english_Subtitulo_plano_tatico_Inbound_Marketing',
					),
					array(
					    'name'    => 'Check List Plano Tático:',
					    'id'      => 'services_english_lis_plano_tatico_Inbound_Marketing',
					    'type'    => 'fieldset_text',

					    'options' => array(
					        'titulo'      => 'Título',
					        'basic'       => 'Texto',
					    ),
					    // Is field cloneable?
					    'clone' => true,
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Foto ilustrativa - Plano Tático',
					    'id'   => 'services_english_ilustrativo_plano_tatico_Inbound_Marketing',
					),
					array(
					    'type' => 'text',
					    'name' => 'Título - Como funciona',
					    'id'   => 'services_english_titulo_como_funciona_Inbound_Marketing',
					),
					array(
					    'type' => 'textarea',
					    'name' => 'Texto - Como funciona',
					    'id'   => 'services_english_texto_como_funciona_Inbound_Marketing',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Foto ilustrativa - Como funciona',
					    'id'   => 'services_english_ilustrativo_como_funciona_Inbound_Marketing',
					),
					array(
					    'name'    => 'List Plano Como funciona:',
					    'id'      => 'services_english_list_como_funciona_Inbound_Marketing',
					    'type'    => 'fieldset_text',

					    'options' => array(
					        'titulo'      => 'Título',
					        'texto'       => 'Texto',
					    ),
					    // Is field cloneable?
					    'clone' => true,
					     'max_clone' => '6',

					),
					array(
					    'name'    => 'List ícone:',
					    'id'      => 'services_english_list_icone_como_funciona_Inbound_Marketing',
					    'type'    => 'single_image',

					    'options' => array(
					        'titulo'      => 'Título',
					    ),
					    // Is field cloneable?
					    'clone'     => true,
					    'max_clone' => '6',
					),
					array(
					    'type' => 'text',
					    'name' => 'Título css',
					    'id'   => 'services_english_titulo_360_Inbound_Marketing',
					),
					array(
					    'type' => 'textarea',
					    'name' => 'Texto 360°',
					    'id'   => 'services_english_texto_360_Inbound_Marketing',
					),
					array(
					    'type' => 'text',
					    'name' => 'Lista 360°',
					    'id'   => 'services_english_list_360_Inbound_Marketing',
					    'clone'     => true,

					),
					array(
					    'type' => 'single_image',
					    'name' => 'Icone canais estratégia',
					    'id'   => 'services_english_icone_canais_estrategia_Inbound_Marketing',
					),
					array(
					    'type' => 'wysiwyg',
					    'name' => 'Texto canais estratégia',
					    'id'   => 'services_english_texto_canais_estrategia_Inbound_Marketing',
					),
					array(
					    'type' => 'text',
					    'name' => 'Título - Qualquer negócio',
					    'id'   => 'services_english_titulo_qualquer_negocio_Inbound_Marketing',
					),
					array(
					    'type' => 'wysiwyg',
					    'name' => 'Texto - Qualquer negócio esquerda',
					    'id'   => 'services_english_texto_qualquer_negocio_esquerdo_Inbound_Marketing',
					),
					array(
					    'type' => 'wysiwyg',
					    'name' => 'Texto - Qualquer negócio direita',
					    'id'   => 'services_english_texto_qualquer_negocio_direita_Inbound_Marketing',
					),
					array(
					    'type' => 'text',
					    'name' => 'Frase título rodapé',
					    'id'   => 'services_english_frase_titulo_rodape_Inbound_Marketing',
					),
					array(
					    'type' => 'wysiwyg',
					    'name' => 'Texto título rodapé',
					    'id'   => 'services_english_frase_texto_rodape_Inbound_Marketing',
					),
				),
			);

			// METABOX DE Social Media Marketing
			$metaboxes[] = array(

				'id'			=> 'detalhes_metabox_social_Media_Marketing',
				'title'			=> 'Detalhes Social Media Marketing',
				'pages' 		=> array( 'servico_english' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Frase Subtítulo: ',
						'id'    => "{$prefix}services_english_subtitulo_social_Media_Marketing",
						'desc'  => 'Frase Yes, we are online and now what?',
						'type'  => 'text'
					),

					array(
						'name'  => 'Texto Social Media: ',
						'id'    => "{$prefix}services_english_texto_social_Media_Marketing",
						'desc'  => 'Texto lado esquerdo',
						'type'  => 'wysiwyg'
					),
					array(
						'name'  => 'Foto Social Media: ',
						'id'    => "{$prefix}services_english_foto_social_Media_Marketing",
						'type'  => 'single_image'
					),
					array(
						'name'  => 'Foto Social Media - SMM : ',
						'id'    => "{$prefix}services_english_foto_social_Media_Marketing_SMM",
						'desc'  => 'Foto facebook esquerda',
						'type'  => 'single_image'
					),
					array(
						'name'  => 'Social Media - SMM Título : ',
						'id'    => "{$prefix}services_english_foto_social_Media_Marketing_SMM_titulo",
						'desc'  => 'Título facebook esquerda',
						'type'  => 'text'
					),
					array(
						'name'  => 'Social Media - SMM Texto : ',
						'id'    => "{$prefix}services_english_foto_social_Media_Marketing_SMM_texto",
						'desc'  => 'Texto facebook esquerda',
						'type'  => 'wysiwyg'
					),
					array(
						'name'  => 'Título Box - Facebook ADS : ',
						'id'    => "{$prefix}services_english_foto_social_Media_Marketing_box_titulo",
						'desc'  => 'Título box',
						'type'  => 'text'
					),
					array(
						'name'  => 'Texto Box - Facebook ADS : ',
						'id'    => "{$prefix}services_english_foto_social_Media_Marketing_box_texto",
						'desc'  => 'Texto box',
						'type'  => 'textarea'
					),
					array(
						'name'  => 'Título Instagram - Instagram ADS : ',
						'id'    => "{$prefix}services_english_foto_social_Media_Marketing_Instagram_titulo",
						'desc'  => 'Título Instagram',
						'type'  => 'text'
					),
					array(
						'name'  => 'Texto  - Instagram ADS : ',
						'id'    => "{$prefix}services_english_foto_social_Media_Marketing_Instagram_texto",
						'desc'  => 'Texto Instagram',
						'type'  => 'textarea'
					),
					array(
						'name'  => 'Foto Instagram Social Media: ',
						'id'    => "{$prefix}services_english_foto_social_Media_Marketing_Instagram_foto",
						'type'  => 'single_image'
					),
					array(
						'name'  => 'Frase Rodapé: ',
						'id'    => "{$prefix}services_english_foto_social_Media_Marketing_frase_odape",
						'type'  => 'textarea'
					),
					array(
						'name'  => 'Link texto Rodapé: ',
						'id'    => "{$prefix}services_english_foto_social_Media_Marketing_frase_link_texto",
						'type'  => 'text'
					),
					array(
						'name'  => 'Link Rodapé: ',
						'id'    => "{$prefix}services_english_foto_social_Media_Marketing_frase_link",
						'type'  => 'text'
					),
				),
			);


			// METABOX DE PAID SEARCH
			$metaboxes[] = array(

				'id'			=> 'detalhes_metabox_paid_search',
				'title'			=> 'Detalhes Paid Search',
				'pages' 		=> array( 'servico_english' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
					    'type' => 'text',
					    'name' => 'Título o que é?',
					    'id'   => "{$prefix}services_english_paid_search_titulo_oque_e",
					    'desc'   => 'Adicione um <br> paa quebra de linha',
					),

					array(
					    'name' => 'Texto o que é?',
					    'id'   => "{$prefix}services_english_paid_search_texto_oque_e",
					    'type' => 'wysiwyg',
					),
					array(
					    'name' => 'Texto lado direito',
					    'id'   => "{$prefix}services_english_paid_search_texto_direito",
					    'type' => 'text',
					),
					array(
					    'name'             => 'Logo lado direito',
					    'id'               => "{$prefix}services_english_paid_search_logo_direito",
					    'type'             => 'image_advanced',
					    'max_file_uploads' => 4,
					),
					array(
					    'name' => 'Título - benefícios',
					    'id'   => "{$prefix}services_english_paid_search_titulo_beneficios_direito",
					    'type' => 'text',
					),
					array(
					    'name'  => 'Lista de benefícios',
					    'id'    => "{$prefix}services_english_paid_search_lista_beneficios_direito",
					    'type'  => 'text',
					    'clone' => true,
					),
					array(
					    'name'             => 'imagem ilustrativa',
					    'id'               => "{$prefix}services_english_paid_search_img_ilustrativa",
					    'type'             => 'image_advanced',
					    'max_file_uploads' => 1,
					),
					array(
					    'name'             => 'Como funciona?',
					    'id'               => "{$prefix}services_english_paid_search_texto_como_funciona",
					    'type'             => 'wysiwyg',
					),
					array(
					    'name'             => 'Ciclo compra',
					    'id'               => "{$prefix}services_english_paid_search_titulo_ciclo",
					    'type'             => 'textarea',
					),
					
					array(
					    'id'      => 'services_english_paid_search_list_ciclo',
					    'name'    => 'Lista ciclo',
					    'type'    => 'fieldset_text',

					    // Options: array of key => Label for text boxes
					    // Note: key is used as key of array of values stored in the database
					    'options' => array(
					        'titulo'    => 'Titulo',
					        'texto'     => 'Texto',
					    ),



					    // Is field cloneable?
					    'clone' => true,
					),
					array(
					    'name'             => 'How It Works? - Título',
					    'id'               => "{$prefix}services_english_paid_search_titulo_How_It_Works",
					    'type'             => 'text',
					),
					array(
					    'name'             => 'How It Works? - Texto',
					    'id'               => "{$prefix}services_english_paid_search_texto_How_It_Works",
					    'type'             => 'textarea',
					),
					array(
					    'name'             => 'Ciclo PPC Handgran - Título',
					    'id'               => "{$prefix}services_english_paid_search_titulo_Ciclo_PPC",
					    'type'             => 'text',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Planejamento - Imagem',
					    'id'   => 'services_english_paid_search_img_Ciclo_Planejamento',
					),
					array(
					    'name'             => 'Planejamento - Título',
					    'id'               => "{$prefix}services_english_paid_search_titulo_Ciclo_Planejamento",
					    'type'             => 'text',
					),
					array(
					    'name'             => 'Planejamento - Lista',
					    'id'               => "{$prefix}services_english_paid_search_lista_Ciclo_Planejamento",
					    'type'             => 'text',
					    'clone'             => true,
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Criação - Imagem',
					    'id'   => 'services_english_paid_search_img_Ciclo_Criacao',
					),
					array(
					    'name'             => 'Criação - Título',
					    'id'               => "{$prefix}services_english_paid_search_titulo_Ciclo_Criacao",
					    'type'             => 'text',
					),
					array(
					    'name'             => 'Criação - Lista',
					    'id'               => "{$prefix}services_english_paid_search_lista_Ciclo_Criacao",
					    'type'             => 'text',
					    'clone'             => true,
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Otimização - Imagem',
					    'id'   => 'services_english_paid_search_img_Ciclo_Otimizacao',
					),
					array(
					    'name'             => 'Otimização - Título',
					    'id'               => "{$prefix}services_english_paid_search_titulo_Ciclo_Otimizacao",
					    'type'             => 'text',
					),
					array(
					    'name'             => 'Otimização - Lista',
					    'id'               => "{$prefix}services_english_paid_search_lista_Ciclo_Otimizacao",
					    'type'             => 'text',
					    'clone'             => true,
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Mensuração - Imagem',
					    'id'   => 'services_english_paid_search_img_Ciclo_Mensuracao',
					),
					array(
					    'name'             => 'Mensuração - Título',
					    'id'               => "{$prefix}services_english_paid_search_titulo_Ciclo_Mensuracao",
					    'type'             => 'text',
					),
					array(
					    'name'             => 'Mensuração - Lista',
					    'id'               => "{$prefix}services_english_paid_search_lista_Ciclo_Mensuracao",
					    'type'             => 'text',
					    'clone'             => true,
					),
					array(
					    'name'             => 'Our goals for your PPC',
					    'id'               => "{$prefix}services_english_paid_search_Ourgoals_titulo",
					    'type'             => 'text',
					),
					array(
					    'name'             => 'Our goals for your PPC Lista',
					    'id'               => "{$prefix}services_english_paid_search_Ourgoals_lista",
					    'type'             => 'text',
					    'clone'            => true,
					),
					array(
					    'name'             => 'Título Rodapé',
					    'id'               => "{$prefix}services_english_paid_search_titulo_rodape",
					    'type'             => 'text',
					),
					array(
					    'name'             => 'Texto Rodapé',
					    'id'               => "{$prefix}services_english_paid_search_texto_rodape",
					    'type'             => 'textarea',
					),
					array(
					    'name'             => 'Link Rodapé',
					    'id'               => "{$prefix}services_english_paid_search_link_rodape",
					    'type'             => 'text',
					),

				),
			);

			// METABOX DE VAGAS
			$metaboxes[] = array(

				'id'			=> 'detalhes_metabox_design_naming',
				'title'			=> 'Detalhes Design Naming',
				'pages' 		=> array( 'servico_english' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
					    'name' => 'Imagem ilustrativa',
					    'id'   => 'services_english_design_naming_Imagem_ilustrativa_texto',
					    'type' => 'single_image',
					),
					array(
					    'name' => 'Design Naming -  o que é?',
					    'id'   => "{$prefix}services_english_design_naming_texto",
					    'type' => 'wysiwyg',
					),
					array(
					    'name' => 'Imagem ilustrativa - Box Cinza',
					    'id'   => 'services_english_design_naming_Imagem_ilustrativa_box',
					    'type' => 'single_image',
					),
					array(
					    'name' => 'Título - Box Cinza',
					    'id'   => 'services_english_design_naming_titulo_box',
					    'type' => 'text',
					),
					array(
					    'name' => 'Texto - Box Cinza',
					    'id'   => 'services_english_design_naming_texto_box',
					    'type' => 'textarea',
					),
					array(
					    'name' => 'Título - área logo',
					    'id'   => 'services_english_design_naming_titulo_area_logo',
					    'type' => 'text',
					),
					array(
					    'name' => 'Texto - área logo',
					    'id'   => 'services_english_design_naming_texto_area_logo',
					    'type' => 'text',
					),
					array(
					    'name' => 'Título Rodapé',
					    'id'   => '{$prefix}services_english_design_naming_titulo_rodape',
					    'type' => 'text',
					),
					array(
					    'name' => 'Texto Rodapé',
					    'id'   => '{$prefix}services_english_design_naming_texto_rodape',
					    'type' => 'textarea',
					),
					array(
					    'name' => 'Link Rodapé',
					    'id'   => '{$prefix}services_english_design_naming_link_rodape',
					    'type' => 'text',
					),
				),
			);
    	    
    	    // METABOX DE VAGAS
			$metaboxes[] = array(

				'id'			=> 'detalhes_metabox_vaga_gran',
				'title'			=> 'Detalhes das Vagas Gran',
				'pages' 		=> array( 'vaga' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
					    'type' => 'text',
					    'name' => 'Breve detalhe da vaga',
					    'id'   => 'vaga_breve_detalhe',
					    'desc'   => 'Breve detalhe da Vaga',
					),
					array(
					    'type' => 'textarea',
					    'name' => 'Descricao da Vaga',
					    'id'   => 'vaga_descricao',
					    'desc'   => 'Mais detalhes da vaga',
					),
					array(
					    'type' => 'single_image',
					    'name' => 'Foto da Vaga',
					    'id'   => 'vaga_descricao_foto',
					    'desc' => 'Foto vaga',
					),
					
				),
			);




    	return $metaboxes;

		}


	function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

    /****************************************************
	* AÇÕES
	*****************************************************/

	

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseGran');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseGran();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );

?>